Docker で ASP.NET Core アプリをコンテナ上で動かしてみる
====

この記事は Advent Calendar 2016 Extra Docker の3日目の記事です。 

2日目の記事では、 Dockerfile を作って自分用のイメージをビルド、さらにそこからコンテナを作成しました。

過去2回、 Nginx という Web サーバーを題材にしてきましたが、今回はちょっと気分転換にプログラミングの話をしようと思います。  
皆さんにもなじみの深い、 Visual Studio を使った内容です。

# ASP.NET Core
今年の7月に、 .NET ファミリーの一員として、 .NET Core がリリースされました。
.NET Core は、**クロスプラットフォーム**で動く .NET です。  

もともと .NET Framework は Windows 上でしか動作しませんでした。  
それを、ほかの OS でも動くように、 Windows 依存部分を切り離したりしてできたのが .NET Core。

.NET Core の上で構築した Web アプリケーションフレームワーク、それが ASP.NET Core です。

****

最近は .NET Standard というのも出てきたりして、 .NET ファミリーが複雑すぎてよくわかりません。  
よって詳しい説明は省きます。

↓の記事が読みやすかったです。  
[さいきんの.NETのこととかNuGetとかCoreとかよく分からないよねーって話 - Qiita](http://qiita.com/acple@github/items/e80bef939583fc2b0e5e)

****

事実として

* OBIC Portal というプロジェクトでは ASP.NET Core を使っています
* ASP.NET Core で作った Web アプリケーションは、 Docker コンテナ上で動きます

ということなので、今回はいろいろと端折って「Docker コンテナ上で ASP.NET Core のアプリケーションを動かす」という部分だけやります。

# Visual Studio 2015
Visual Studio 2015 は、 ASP.NET Core アプリ開発に対応しています。  

[.NET - Powerful Open Source Development](https://www.microsoft.com/net/core#windowsvs2015)

こちらから、 .NET Core アプリ開発に必要なツール類をインストールするだけで OK。  
本題はここじゃないので、設定は省略。

## Visual Studio Tools for Docker
VS の拡張機能に「Visual Studio Tools for Docker」というものがあります。  
Docker とついているので、とりあえずインストールしてみましょう。

![](images/vs-tools-01.png)

![](images/vs-tools-02.png)

## ASP.NET Core Web アプリケーションの作成
### プロジェクトの作成
ASP.NET Core Web アプリケーションを作ります。

いつも通り、 ファイル > 新規作成 > プロジェクト からテンプレートを選びます。

![](images/vs-01.png)

さらにテンプレートを選ぶ画面になるので、 「Web アプリケーション」を選択

![](images/vs-02.png)

出来ました。

![](images/vs-03.png)

### Docker Support の追加
プロジェクトを右クリックしてコンテキストメニューを表示 > 追加  
すると、「Docker Support」 という項目があります。

![](images/vs-04.png)

先ほどインストールした「Visual Studio Tools for Docker」によって拡張された機能です。

追加すると

![](images/vs-05.png)

`docker～` とついたファイルが増えます。

![](images/vs-06.png)

そして、お気づきでしょうか。

![](images/vs-06-2.png)

「IIS Express」 と表示されてたところが 「Docker」 に変わりました。

## デバッグ実行
プロジェクトの準備ができたので、 F5 を押してデバッグ実行します。

![](images/vs-07.png)

なんかエラーが出たので、ちょっと設定変更。

右下（Macの場合は右上）にいる Docker のクジラマークを右クリックして  
![](images/docker-for-win-01.png)  
Settings を開きます。

![](images/docker-for-win-02.png)

Shared Driver でプロジェクトを作ったドライブにチェックをつけて Apply。  

これで、準備OK。
再度 F5 を押します。

![](images/vs-08.png)

Webアプリが立ち上がりました。

これ、 **Docker のコンテナ上で動いてます。**

## ほんとにコンテナ上で動いてるの？
昨日までコマンドラインで操作する話をしてたのに、今回なにもしてないけど本当にコンテナ上でうごいてるの？  

というわけで、コマンドをたたいてみます。

```sh
$ docker ps
```

```
CONTAINER ID        IMAGE                        COMMAND               CREATED             STATUS              PORTS                   NAMES
9f0fd9b7dead        user/vsdockersampleapp:dev   "tail -f /dev/null"   2 minutes ago       Up 2 minutes        0.0.0.0:32768->80/tcp   vsdockersampleapp_vsdockersampleapp_1
```

コンテナができていました。

ついでに

```
$ docker images
```

をたたいてみると

```
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
user/vsdockersampleapp   dev                 1bb2bbb6c14a        12 minutes ago      266.8 MB
microsoft/aspnetcore     1.0.1               fac396c25a9b        22 hours ago        266.8 MB
d4w/nsenter              latest              9e4f13a0901e        9 weeks ago         83.85 kB
```

イメージが出来上がってます。

`microsoft/aspnetcore` が、もとになったイメージで `user/vsdockersampleapp` が今回作られたイメージです。  
※ `d4w/nsenter` はよくわかりませんが、たぶんデバッグ実行をサポートするためのイメージ

これらは、 F5 を押してアプリケーションが起動するまでの間に、 Visual Studio がいい感じで自動でやってくれた作ってくれたものです。

これで、 ASP.NET Core アプリをコンテナ上で動かしてみることができました！  
ステップ実行もできます。

VS すごいな…

# Visual Studio Code
VS の拡張機能がよしなにやってくれすぎて、ASP.NET Core アプリをコンテナ上で動かしてみた感があまりなかったので、もう一つ別のことをやってみます。

今回は Visual Studio Code を使ってみます。

Visual Studio Code は、クロスプラットフォームでうごく Visual Studio で、オープンソースで開発されています。

Visual Studio 2015 が「統合開発環境（IDE）」なのに対して、Visual Studio Code は「高機能エディタ」です。  
メモ帳や、さくらエディタとかの仲間。

## VS Code のインストール
[https://code.visualstudio.com](https://code.visualstudio.com) からインストーラーをダウンロードしてインストーラー実行すればOK。

## .NET Core SDK のインストール 
[https://www.microsoft.com/net/core#windowscmd](https://www.microsoft.com/net/core#windowscmd)

![](images/dotnet-sdk-01.png)

ここの 「Command line / Other」 から、 .NET Core 1.1 SDK をダウンロードしてインストールします。

※すみません、 macOS は省略します。Macのタブに従ってインストールすれば OK です。

****

もともと .NET Core は `dotnet-cli` というコマンドラインツールが提供されてます。（ここでもコマンドライン！）
.NET Core 1.1 SDK はさらに開発関連のサブコマンドを追加した機能です。

インストールが完了したら、コマンドプロンプトなどから

```sh
$ dotnet --help
```

と打ってみてください。

```
.NET Command Line Tools (1.0.0-preview2-1-003177)
Usage: dotnet [host-options] [command] [arguments] [common-options]

Arguments:
  [command]             The command to execute
  [arguments]           Arguments to pass to the command
  [host-options]        Options specific to dotnet (host)
  [common-options]      Options common to all commands

Common options:
  -v|--verbose          Enable verbose output
  -h|--help             Show help

Host options (passed before the command):
  -v|--verbose          Enable verbose output
  --version             Display .NET CLI Version Number
  --info                Display .NET CLI Info

Common Commands:
  new           Initialize a basic .NET project
  restore       Restore dependencies specified in the .NET project
  build         Builds a .NET project
  publish       Publishes a .NET project for deployment (including the runtime)
  run           Compiles and immediately executes a .NET project
  test          Runs unit tests using the test runner specified in the project
  pack          Creates a NuGet package
```

Common Commands にあるようなことが、コマンドラインでできるようになります。

****

新規 Web プロジェクトを作る  
```
$ dotnet new -t web
```

依存パッケージを復元する（Nuget と同じ）  
```
$ dotnet restore
```

プロジェクトを実行する
```
$ dotnet run
```

****

などなど

## node.js、 npm
`dotnet new -t web` で、Web アプリケーションを作ることができます。  
VS 2015 で、新規プロジェクトを作成したみたいに。  
出来上がるプロジェクトも、 VS 2015 でやった時と同じです。

これはこれで試してもらいたいのですが、今回はそれとは別の方法で、新規 Web プロジェクトを作ります。 

そのために Node.js をインストールします。

[https://nodejs.org](https://nodejs.org)

Node.js は、 サーバーサイドで JavaScript を実行する環境です。  
今回は、サーバーサイドは ASP.NET Core なので、 Node.js 自体は使いません。

Node.js のパッケージ管理システムである npm を使いたいので、 Node.js をインストールします。  
※ .NET に対する Nuget、 Node.js に対する npm

![](images/nodejs-01.png)

さいとから推奨版をダウンロード。

![](images/nodejs-02.png)

インストールするさいに、 npm package manager が含まれていることをよく確認しておいてください。

****

インストール完了後、コマンドラインでインストールされているか確認してみます。

```
$ node -v
v6.9.1
```

```
$ npm -v
4.0.2
```

## Yeoman
Web アプリケーションを新規で作るために、 Yeoman をインストールします。
「よーまん」と読みます。

Yeoman は、コマンドラインで対話的に新規プロジェクトを生成してくれるツールです。

`npm` コマンドを使ってインストールします。

```sh
$ npm install -g yo
```

Yeoman で使う ASP.NET のテンプレートもインストールします。

```
$ npm install -g generator-aspnet
```

そして `yo` コマンドを↓のように打ってみてください。

```
$ yo aspnet
```

![](images/vscode-01.png)

アスキーアートのおっさんが登場し、どのタイプのプロジェクトを作りたいか聞いてきます。  
質問に答える形で、タイプやプロジェクト名を打っていくと新規プロジェクトが出来上がります。

VS 2015 では、マウスでポチポチと新規プロジェクトのテンプレートを選択していきましたが  
Yeoman を使うと、なんとコマンドラインだけでそれができちゃうわけです。

## VS Code でプロジェクトを開く
`yo` コマンドで新規プロジェクトを作ると、プロジェクト名のフォルダ以下にファイルが配置されます。

なので

```
$ cd /プロジェクト名
```

でその階層に移動。

```
$ code .
```

と打つと、VS Code が立ち上がります。

![](images/vscode-02.png)

## ビルドする

上の画像でお気づきだと思いますが、 Yeoman によって作られた Web プロジェクトは、 Dockerfile が最初からついてきています。
なので、あとは2日目の記事と同じように `docker build`、 `docker run` の流れです。

```
$ docker build -t vscodesample .
```

```
$ docker run -d -p 8080:5000 vscodesample
```

`docker run` で `-p 8080:5000` 指定しています。  
Dockerfile の中身を見ると

```
EXPOSE 5000/tcp
```

という書いてあるので、「コンテナ側は 5000 番ポートで通信のやり取りをするよ」となっています。
なので、ローカルの 8080 番ポートをコンテナの 5000 番ポートに紐づけてあげます。

****

で、`http://localhost:8080` にブラウザからアクセスすると

![](images/vscode-03.png)

無事 Web アプリケーションが立ち上がってました。

## ほんとにコンテナ上で動いてるの？
VS 2015 と違って、 `docker build`、 `docker run` を自分でやったので確認するまでもありませんが、一応。

```
$ docker images
```

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
vscodesample        latest              0bf92fb316bf        3 minutes ago       810.7 MB
microsoft/dotnet    latest              b6736a797bba        23 hours ago        608.5 MB
```

```
$ docker ps
```

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
1b226750b99b        vscodesample        "dotnet run --server."   2 minutes ago       Up 2 minutes        0.0.0.0:8080->5000/tcp   hopeful_bardeen
```

VS 2015 との違いは結構あります。  
ぜひ、見比べてみてください。（今回は割愛）

あと、残念ながら VS Code からはコンテナ上の Web アプリケーションをステップ実行することはできません。  
テキストエディターなので。

コンテナ上でなければ、デバッグ、ステップ実行ができます。（テキストエディターなのに！）

# まとめ
VS 2015、 VS Code の両方で ASP.NET Core の Web アプリケーションを**コンテナ上で**実行することができました。

VS 2015 はさすがと言うか何というか、ほぼ Docker を意識しないうちに終わってましたね。

「VS 2015 が自動でやっていてくれたことを自分でやると」というのが、 VS Code でやったことになります。

どちらがいいかはお好みで。

VS Code のようなテキストエディタとコマンドラインの組み合わせでシステムを構築してみるのも、たまには良いと思います。  
裏側がどうなっているのか次第にわかってくるので。

ぜひ、 VS Code もさわって見てください。

****

ちなみに、 VS Code も拡張機能によっていろいろな機能を追加できます。  
C# だけじゃなく、 Go言語 や ruby などいろいろな開発言語の補助をしてくれたり、  
Dockerfile や、 昨日ちょっと触れた nginx.conf もシンタックスハイライトしてくれたり。

SQL も書けたり。

テキストエディタとしては相当高機能です。

私は最近、ほとんど VS を立ち上げてません。全部 VS Code でやってます。

この記事も VS Code で書いてたり。

![](images/dasoku.png)

**VS Code はいいぞ**