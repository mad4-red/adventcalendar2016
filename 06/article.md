開発環境の話まとめ
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の6日目の記事です。 

[5日目の記事](http://tkysva448/Hinata/item/22b0e9ab20884fc58073ce5fbe680758)ではコンテナのライフサイクルとデータの永続化について取り上げました。  
ここまでの内容で、Dockerに「触れる」ための基礎部分はだいたい押さえた形です。

連日の記事で若干息切れ気味なので今回はライトにまとめて  
明日以降、新たなステージに移る布石にしたいと思います。

# ここまでの話のまとめ
## インストール
* Windows なら [Docker for Windows](https://docs.docker.com/docker-for-windows/)
    - クライアント Hyper-V が使えなかったら [Docker Toolbox](https://github.com/docker/toolbox/releases/tag/v1.12.3)
* Mac なら [Docker for Mac](https://docs.docker.com/docker-for-mac/)
* インストーラーで簡単にインストールできる

## Docker の基本コマンド
* `docker pull` でイメージを取得
* `docker run` でイメージからコンテナを起動
    - `-p` オプションでポートフォワーディング
    - `-e` オプションで環境変数を指定
    - `--link` オプションでほかのコンテナにつなぐ
    - `-v` オプションでボリュームをマウントする
        - 昨日「マッピング」と記載しましたが、正確には「マウント」
* `docker ps`
    - 動いているコンテナの一覧を出力する
* `docker stop`
    - コンテナを一時停止する
* `docker start`
    - 止まっているコンテナを開始する
* `docker rm`
    - コンテナを削除する
    - `-f` 動いているコンテナを強制的に削除する
* `docker build`
    - Dockerfile からイメージを作成する
* [他にもあります](https://docs.docker.com/engine/reference/commandline/#/container-commands)

## Dockerfile
* イメージを自分で定義する
* `FROM`
    - もとになるイメージを指定
* `COPY`
    - コンテナの中に持っていくファイルを指定
* `EXPOSE`
    - コンテナの外とやり取りするポート番号を指定
* `CMD`
    - `docker run` するときに実行するコマンドを指定
* [他にもあります](https://docs.docker.com/engine/reference/builder/)

## ASP.NET Core & Visual Studio
* ASP.NET Core のアプリケーションは、コンテナ上で動く
* Visual Studio の拡張機能「[Visual Studio Tools for Docker ](https://marketplace.visualstudio.com/items?itemName=MicrosoftCloudExplorer.VisualStudioToolsforDocker-Preview)」を使うと、Docker やコンテナを意識せずに開発できる

## ボリューム
* コンテナが削除されると、コンテナ内のデータも消えてなくなる
* 永続化したいデータはボリュームを別に作ってそこにマウントする
* `docker volume create` でボリュームを作成する
* `docker run -v` でボリュームをコンテナにマウントする

# ここからの話
ここからようやく **Docker とはなにか** という話に踏み込んでいこうと思います。

そのうえで、一つ前提知識を共有しておきます。

**Docker は Linux のコンテナ型仮想化技術を簡単に使うためのツールである。**

****
<small>最近はそういうわけでもないのですが、いったんそういう認識でいてください。ややこしい話ですが…</small>
****

なので、 Linux の環境がないと Docker は動かせません。

じゃあ、 Windows や Mac にインストールした Docker は何なのかという話ですが  
実はあれ、「Windows や Mac の上に Linux の環境を作って、そこで Docker を動かしていた」んです。

図にするとこんな感じ

![](images/docker-01.png)

![](images/docker-02.png)

Windows や macOS の上に Linux が乗っている環境とは？  

![](images/docker-03.png)

****

この環境を成立させるのが、 **サーバー仮想化**の技術です。  
**コンテナ** への理解を深めるため、明日は Docker ネタから離れて、仮想化技術について取り上げたいと思います。

短いですが今日はこの辺で。