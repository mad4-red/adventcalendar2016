OBIC Portal と Docker ～運用監視～
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の24日目の記事です。 

いよいよこのカレンダーもラスト2日となりました。  
今日の記事は、 OBIC Portal というサービスを維持するための「運用監視」に焦点を当ててみます。

サービスを提供するということは、ユーザーがサービスを適切に利用する状況を保証しなければいけません。  
それが、24時間365日なのか、平日9時18時なのかは、サービスの特性、契約内容によりますが  
定められた時間は、可能な限りサービスを稼働させ続けなければなりません。  
何か問題が起きたとしても、迅速に対応、復旧することが求められます。

それを実現するために「運用監視」は必須の仕組みです。

**「健全なサービス運営は適切な運用監視から」** です。

# 障害監視
まず、「問題が発生したときに、即座にそれに気づく仕組み」です。

OBIC にも当然、すでにその仕組みは存在します。  
「NS 商材」と呼ばれる「障害監視システム」です。

![](images/shougai-01.png)  
※顧客名を含む部分については、ぼかしを入れてます

障害監視システムで OBIC7 の動いてるサーバーに対して  
「ping が通るか」  
「port が開いていて通信できるか」  
「CPU が異常値になっていないか」  
などの監視しています。

****

OBIC Portal においては、 Swarm mode の「オーケストレーション」機能によって、サービスの「稼働を維持する」部分はかなり担保されています。  
とはいえ、監視しなくていいというわけではありません。

というか、 Docker 1.12 の overlay network がどうも不安定な時があって、きちんとコンテナ間の通信ができないということがありまして…

というわけで、「コンテナの稼働状況を監視する」仕組みを OBIC Portal では構築しています。

## Digdag
「OBIC 障害監視システム」は、 Windows + .NET Framework という前提の仕組みです。  
Linux を使う OBIC Portal ではそのまま使えません。

.NET Core で動くようにするのも手ですが、まずは手間をかけずに OSS を採用します。

****

[Digdag](https://www.digdag.io/)  
![](images/digdag-01.png)

Digdag は 2016年6月に OSS として公開された「ワークフローエンジン」です。  
<small>
ワークフローというと OBIC7 の「承認ワークフロー」を思い出しますが  
一般的なワークフローは「処理の手続きの定義」を指し、ワークフローエンジンは、その定義を実行するエンジンを指します。  
OBIC7のワークフローは「申請・承認」に特化したもの。  
なので、お客様と話すときは「承認ワークフロー」と言わないと誤解を招きがちです。
</small>

Digdag ではワークフローの定義を *Yaml* で定義します。  

```Yaml
timezone: Asia/Tokyo

schedule:
  cron>: 1,6,11,16,21,26,31,36,41,46,51,56 7-22 * * 1-5

_parallel: false

+common:
  _parallel: true

  +proxy:
    sh>: sh/proxy.sh
  +portal:
    sh>: sh/portal.sh
  +manage:
    +app:
      sh>: sh/manage-app.sh
    +api:
      sh>: sh/manage-api.sh
  +notice:
    sh>: sh/notice.sh

+org-app:
  _parallel: true

  +task:
    +app:
      sh>: sh/task-app.sh
    +api:
      sh>: sh/task-api.sh
  +file:
    sh>: sh/file.sh
  +message:
    sh>: sh/message.sh
```

細かい動作を書いているのはシェルスクリプト（バッチファイル）。

```sh
#!/bin/bash
cwd="$(cd `dirname $0` && pwd)"

service_name="notice-api"
service=${service_name}-${OBIC_PORTAL_ORG_MONITOR_ORG_ID}

# ping check
ping ${service} -c 1 >> /dev/null

if [ $? != 0 ]; then
    message="[Failure] [${OBIC_PORTAL_ORG_MONITOR_ORG_DOMAIN}] [${service_name}] ping ${service}"
    echo ${message}
    ${cwd%/}/webhook-notice.sh 2 "${message}"
    exit 1
fi

echo "[Success] [${OBIC_PORTAL_ORG_MONITOR_ORG_DOMAIN}] [${service_name}] ping ${service}"

# curl check
json=`curl -sSf http://${service}/_health`

if [ $? = 7 ]; then
    message="[Failure] [${OBIC_PORTAL_ORG_MONITOR_ORG_DOMAIN}] [${service_name}] Failed to connect (curl)"
    ${cwd%/}/webhook-notice.sh 2 "${message}"
    exit 1
fi

if [ $(echo $json | jq -r ".health") = true ]; then
    echo "[Success] [${OBIC_PORTAL_ORG_MONITOR_ORG_DOMAIN}] [${service_name}] Connectable (curl)"
    exit 0
fi

message=""
while read -r c
do
    name=`echo $c | jq -r ".name"`
    msg=`echo $c | jq -r ".message"`
    message="${message}\n  ${name}: ${msg}"
done < <(echo $json | jq -c ".details | map(select(.health == false)) | .[]")

message="[Warning] [${OBIC_PORTAL_ORG_MONITOR_ORG_DOMAIN}] [${service_name}] Anomaly detection (curl) ${message}"

echo -e "${message}"

${cwd%/}/webhook-notice.sh 2 "${message}"

exit 0
```

エラーが発生したときは、メールや外部サービス（Slack）などに通知を発報します。

## Digdag もコンテナ化

Digdag 自体もコンテナ化して、 Docker の overlay network 内に置きます。  
Docker ネットワーク内に置かないとコンテナと通信できないので。

![](images/digdag-02.png)

****

シェルスクリプトの中身など、細かいところは置いておいて  
今までは「NS 商材」と呼ばれ、OBIC7 とは一線を画していた「障害監視」というサービスも、 OBIC Portal の中身に組み込んでいます。

とはいえ、監視の内容が貧弱だったり、「ホスト自体の障害監視」は未実装なので、まだまだこれから強化していかないといけないところです。

# ログ収集
「障害監視」は「発生したことに対して迅速に対応する」仕組みです。  
もう一つ、運用監視という枠組みで必要なのは、「発生したことを分析し、再発しないように対処する」仕組みです。

そのために「ログ」を収集する必要があります。

OBIC7 でいうところの、 ACondition に収集される A0000RR_Log に当たる機能です。

## Docker コンテナのログ
Docker コンテナ内で起こったことのログは、 `docker logs` コマンドで出力することができます。

nginx のコンテナを立ち上げて、ブラウザでアクセスしたあとのログを見てみます。

```
$ docker run -d -p 8080:80 --name my_nginx nginx
```

```
$ docker logs my_nginx
172.17.0.1 - - [22/Dec/2016:09:53:34 +0000] "GET / HTTP/1.1" 200 612 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36" "-"
172.17.0.1 - - [22/Dec/2016:09:53:34 +0000] "GET /favicon.ico HTTP/1.1" 404 571 "http://localhost:8080/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36" "-"
2016/12/22 09:53:34 [error] 5#5: *1 open() "/usr/share/nginx/html/favicon.ico" failed (2: No such file or directory), client: 172.17.0.1, server: localhost, request: "GET /favicon.ico HTTP/1.1", host: "localhost:8080", referrer: "http://localhost:8080/"
```

nginx へのアクセスログが出力されました。  

仕組みを軽く説明すると  
nginx のログは `/var/log/nginx/access.log` というファイルに出力されます。  
nginx の [Docker イメージ](https://github.com/nginxinc/docker-nginx/blob/844627e5cb58c751f8b43824c7b6ea84529c73bf/mainline/jessie/Dockerfile)では、 `/var/log/nginx/access.log` を `/dev/stdout` という「標準出力」にリンクさせています。  
なので、`/var/log/nginx/access.log` に書き込まれたログは、そのまま標準出力（つまり黒いコンソール画面）に表示されます。

Docker では、標準出力の内容を「コンテナのログ」として扱います。  
`docker logs` で表示するのは、コンテナの標準出力の内容です。

標準出力に何を出力するかは、アプリケーションの作り次第です。  
※.NET Core のアプリケーションも、「ログを標準出力に出す」と指定することができます。

## Fluentd

コンテナごとのログは、 `docker logs コンテナ名` で見ることができました。  
ただ、サービスが拡充されるにつれ、コンテナの数は膨大になります。  
何か問題が発生したときに、「どのコンテナで起きたか」を特定し、「そのコンテナのログを見る」という手順を踏むのは非常に手間です。というか至難の業です。

なので、各コンテナのログは一か所にまとめるように工夫します。

Docker には、「コンテナのログの扱いをプラグインに移譲する」という機能があります。  
そのプラグインの部分を Logging Drivers と呼びます。

Logging Drivers にはいろいろあるのですが、 OBIC Portal では Fluentd を使用しています。

[Fluentd](http://www.fluentd.org/)  
![](images/fluentd-01.png)

「ふるーえんとでぃー」と読みます。

Fluentd 自体は Docker に限らず使えるアプリケーションで、簡単に言うとファイルのフォーマット変換を強力にしたようなものです。  

* 入力の形の定義
* 出力の形の定義

を fluentd.conf というファイルに定義することで、自在な入出力を可能としています。

やろうと思えば、  
「IIS のログファイルを Fluentd にかませて、 SQLServer のテーブルに書き込む」  
なんてこともできます。（たぶん）

****
OBIC Portal では、 Docker コンテナのログを Fluentd Log Driver を介して、 Fluentd に送り込み、  
Fluentd から Elasticsearch という NoSQL データベースに蓄積させています。

![](images/fluentd-02.png)

Fluentd や Elasticsearch は別の「サーバー」でもいいのですが、例にもれず、もちろんコンテナにしています。

![](images/fluentd-03.png)

入出力の定義を書いた fluent.conf も、当然ながら TFS でバージョン管理です。

```fluent
<source>
  type forward
  port 24224
  bind 0.0.0.0
</source>

# 1. IdP、Proxyなど、共通のサービス
# 1-1. レコード追加
# docker.portal.idp
#   -> reformed.docker.portal.idp
<match docker.portal.*>
  type record_reformer
  renew_record false
  enable_ruby false
  tag reformed.${tag}
  <record>
    domain portal # portal
    service ${tag_parts[2]} # idp
  </record>
</match>

# 1-2. Elasticsearch へ送信
# reformed.docker.portal.idp
<match reformed.docker.portal.*>
  type forest
  subtype copy
  <template>
    <store>
      type elasticsearch
      host log_elasticsearch
      port 9200
      logstash_format true # logstash-*
      type_name ${tag_parts[3]} # idp
    </store>
  </template>
</match>

# 2. 組織ごとのサービスにレコードを追加する
# 2-1. レコード追加
# docker.portal.task.534e7417-fdf2-41e6-bfe6-707ff2fe41ad.obic.co.jp
#   -> org.docker.portal.task.534e7417-fdf2-41e6-bfe6-707ff2fe41ad.obic.co.jp
<match docker.portal.*.*.**>
  type record_reformer
  renew_record false
  enable_ruby false
  tag reformed.${tag}
  <record>
    domain ${tag_suffix[4]} # obic.co.jp
    organizationid ${tag_parts[3]} # 534e7417-fdf2-41e6-bfe6-707ff2fe41ad
    service ${tag_parts[2]} # task
  </record>
</match>

# 2-2. Elasticsearch へ送信
# reformed.docker.portal.task.534e7417-fdf2-41e6-bfe6-707ff2fe41ad.obic.co.jp
<match reformed.docker.portal.*.*.**>
  type forest
  subtype copy
  <template>
    <store>
      type elasticsearch
      host log_elasticsearch
      port 9200
      logstash_format true # logstash-*
      type_name ${tag_parts[3]} # task
    </store>
  </template>
</match>

# 3. PostgreSQLなどのバックエンド
# 3-1. レコード追加
# docker.backend.postgres
#   -> reformed.docker.backend.postgres
<match docker.backend.**>
  type record_reformer
  renew_record false
  enable_ruby false
  tag reformed.${tag}
  <record>
    domain backend # backend
    service ${tag_parts[2]} # postgres
  </record>
</match>

# 2-2. Elasticsearch へ送信
# reformed.docker.backend.postgres
<match reformed.docker.backend.**>
  type forest
  subtype copy
  <template>
    <store>
      type elasticsearch
      host log_elasticsearch
      port 9200
      logstash_format true # logstash-*
      type_name ${tag_parts[2]} # postgres
    </store>
  </template>
</match>
```

Fluentd をかませることで、 Elasticsearch 以外のデータストア（PostgreSQL や MongoDB、あるいは SQLServer とか）へのログの出力も容易に変更可能になります。

## ログを一か所に集めるという事
ログを一か所に集めることで、問題が発生したときにその原因を特定することが容易になります。

それ以上に大きなメリットが、「問題が発生しそうな箇所を事前に特定することができる」です。  

OBIC7 を例に。  

「原価計算に非常に時間がかかる」

といったレスポンス系トラブル。

今は、「個社ごとに」トラブルが発生したときにはじめて調査に乗り出すスタンスですが  
あらかじめログが一か所に集められていれば「スロークエリ」になりやすい部分を統計から抽出し、事前に改修を加えることができます。  

また、個社で発生した事象をいち早く他社にも展開することも可能になるでしょう。  
（これは「ロギングだけの枠組み」ではないですが）

****

あるいは、ユーザーの操作ログを分析することで、ユーザー体験向上の手掛かりにもつながります。  
ユーザーが画面を操作するとき、「この項目を入力したあとは、この項目にフォーカスしたほうがミスが少ない」といった分析もやろうと思えばできるはず。  
これは、WEB サービスやWEB 広告などで取り入れられる手法ですが、  
業務システムにおいても、これからは考慮されるべき内容だと思います。  
とくに、「稼働・サポート時間の半減」が目標の昨今においては。

****

OBIC Portal では、まだそこまでに至ってません。  
ただ、今のうちに「ログを集約する」という仕組みを設けておくことで、将来そういった動きを取りやすいようにしています。

# まとめ
* 健全なサービス運営は適切な運用監視から
* Digdag
  - OSS のワークフローエンジン
  - OBIC Portal では、障害監視のツールとして、各ネットワーク内にコンテナとして配置している
* Fluentd
  - ログの入出力を汎用的に行うアプリケーション
  - OBIC Portal では、 Docker コンテナのログを Elasticsearch に集約させるために使っている