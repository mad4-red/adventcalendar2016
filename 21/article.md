OBIC Portal と Docker ～ネットワーク～
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の21日目の記事です。 

今日の記事では、 OBIC Portal のネットワーク周りの話をします。

# ASP.NET Core と Nginx
ネットワークの話をする前に、アプリケーションの構造についてひとつ説明を加えておきます。

ASP.NET のアプリケーション。起動、そしてアクセスの仕方と問われれば、まずイメージするのが IIS。  
OBIC7 の Web アプリケーションを思い浮かべると、発行したアプリを Web サーバーに配置して、 IIS で「アプリケーション」に登録することで、使えるようになります。

これは、 IIS という Web サーバーが、 ASP.NET のアプリケーションとよしなにやり取りをして、外の世界とアプリケーションをつないでくれているわけです。（雑な説明）
<small>中身をあまり理解しないまま、ぽちぽちと管理コンソールをいじれば実現できる仕組みを作るのが Microsoft の強みであり弱み</small>

では、 ASP.NET Core はどうかというと、もちろん、 IIS を経由してアプリケーションを実行することもできますが  
そもそも「クロスプラットフォーム」をうたったフレームワークです。  
Linux や Unix で IIS は動きません。

## Kestrel
というわけで IIS のような Web サーバーがなくても Web アプリケーションとして成り立つように、 ASP.NET Core は「内蔵 Web サーバー」を有しています。  
Kestrel （けすとれる）といいます。

[GitHub - aspnet/KestrelHttpServer: A web server for ASP.NET Core based on libuv.](https://github.com/aspnet/KestrelHttpServer)

ASP.NET Core のアプリケーションは、内部で Kestrel を起動し、 Kestrel を経由して外の世界とやりとり（つまり、リクエストを受け、レスポンスを返す）を行います。  
Kestrel はアプリケーションに内蔵されているので、一見するとアプリケーション自体が Web サーバーの仕組みを持っているような形です。

というわけで、 IIS がない代わりに Kestrel があるから 「Web サーバーがない」という問題は解決  
かというと、そうは問屋が卸さない。

Kestrel は非常にシンプルな Web サーバーで、 HTTP のリクエストとレスポンスをただ捌くだけ。  
「Web サーバー」に求めらるのは、セキュリティであったり、対負荷性能だったり、 HTTP のやり取り以外にもあります。  
なので、 Kestrel のまま外の世界にアプリケーションを放り出すのは非常に危険です。

## Nginx とリバースプロキシ
Kestrel を生のまま外には出せない。  
なので外の世界とのやり取りには、 本来の Web サーバーをかませます。

![](images/reverse-proxy-01.png)

別の Web サーバーとのリクエスト・レスポンスを仲介する機能を「リバースプロキシ」と呼びます。
高機能な Web サーバーをリバースプロキシサーバーとしてアプリケーションの前に置くことで  
DOS 攻撃を捌いたり、不正アクセスを防ぐなど、堅牢なシステムを構築することができます。

リバースプロキシが非常に得意な Web サーバーが、いままでの記事にも何度か登場した Nginx です。

![](images/reverse-proxy-02.png)

リバースプロキシの仕組みは、当然コンテナの世界にも持ち込めます。

![](images/reverse-proxy-03.png)

OBIC Portal では、 Nginx コンテナをアプリケーションの前段におくことで  

* アプリケーションはアプリケーションの責任範囲に注力
* Nginx は Web サーバーとしての責任範囲に注力

できる構成にしています。

# overlay network
本題のネットワークの話。

OBIC Portal の特性は

「ユーザーごとに OBIC 社員がユーザーとコミュニケーションする空間」

です。

なので、ユーザーごとの空間、閉じられたネットワークを作る必要があります。

そこで登場するのが、[15日目の記事](http://tkysva448/Hinata/item/36e6912ec06746c9bc38a505dc39801a)にも登場した、 Docker Swarm mode の *overlay network* です。

## ユーザーごとに overlay network を用意する
ではここで、「〇×株式会社」と「中森金属」というユーザーに OBIC Portal を使って頂くことを想定してみます。  
<small>OBIC7 のサンプル取引先マスタでよく見る会社な気がするんですけど、誰が最初につけたんでしょうね？</small>

ユーザーごとに閉じられたネットワークを作るために、 overlay network を作ります。

```
$ docker network create --driver overlay marubatsu-net
$ docker network create --driver overlay nakakin-net
```

![](images/network-01.png)

## ユーザーごとのネットワークにアプリケーションのコンテナを配置する
次は、[昨日の記事](http://tkysva448/Hinata/item/03d181354beb450e80f92cab7c9b7c8b)で作った、アプリケーションごとのコンテナをネットワーク内に配置します。

```
$ docker service create --name message-matubatsu --network marubatsu-net portal/message
$ docker service create --name file-matubatsu --network marubatsu-net portal/file
$ docker service create --name task-matubatsu --network marubatsu-net portal/task

$ docker service create --name message-nakakin --network nakakin-net portal/message
$ docker service create --name file-nakakin --network nakakin-net portal/file
$ docker service create --name task-nakakin --network nakakin-net portal/task
```

Swarm mode なので、 `run` ではなく、 `service create` です。  
サービス名は、ネットワークが別でも Docker 内で一意である必要があるので、アプリ名の後ろに会社名を付けた形にしておきます。

![](images/network-02.png)

## リバースプロキシを用意する
このままだとアプリケーションのコンテナは、ユーザーごとのネットワークから外に出てこれないので、外とのやり取りをするリバースプロキシコンテナを用意します。  

```
$ docker service create --name proxy-matubatsu --network marubatsu-net portal/user-proxy
$ docker service create --name proxy-nakakin --network nakakin-net portal/user-proxy
```

![](images/network-03.png)

*proxy-matubatsu* や *proxy-nakakin* を外の世界につなげば完成といいたいところですがそうもいきません。

例えば、 `http://portal.obic.net` のようなドメインを取得して OBIC Portal につなげるとします。  
その場合、ユーザーごとの環境につなぐには `http://portal.obic.net/marubatsu/message` のような URL でつなぎたいですよね。

上図のままだと、まだ「各ユーザーのネットワークに振り分ける」部分がありません。  
*proxy-matubatsu* や *proxy-nakakin* を外の世界につなぐだけだったら「ポートフォワーディング」でできなくもないですが…  
`http://portal.obic.net:8001` は〇×株式会社、`http://portal.obic.net:8002` は中森金属とやるのはちょっとナンセンス。ポート番号も枯渇してしまいます。

なので、ポート番号 80 や 443 などで受けつつ、サブディレクトリの文字列から各ネットワークに振り分けるために  
もう一つネットワークとリバースプロキシサーバーを用意します。

まずは、ネットワークの追加。  
これこそが外の世界とつながるネットワークということで *public-net* と名付けてみます。
```
$ docker network create --driver overlay public-net
```

リバースプロキシサーバーはポートフォワーディングで外の世界とつなげます。  
ここでは *proxy-public* と名付けてみます。
```
$ docker service create -p 80:80 --name proxy-public --network public-net portal/public-proxy
```

さらに、 *proxy-public* から *proxy-matubatsu* や *proxy-nakakin* にアクセスできるよう、ユーザー側のリバースプロキシサーバーを *public-net* にも所属させます。  
コンテナは複数のネットワークにまたがって作成することができるんです。


```
$ docker service create --name proxy-matubatsu --network public-net --network marubatsu-net portal/user-proxy
$ docker service create --name proxy-nakakin --network public-net --network nakakin-net portal/user-proxy
```

![](images/network-04.png)

## Swarm join
このネットワークが現在の開発環境、つまり 1 台の Linux サーバーで構成されています。

いまのところ 1 台ですが、 Docker Swarm mode を前提に組んだことで、簡単に拡張することができるようになっています。

overlay network は、ホストをまたがったネットワークを実現してくれるので

![](images/network-05.png)

こんな風に、ユーザーが増えてリソースが足りなくなったとしても、 `docker swarm join` で簡単にホストを増やせます。

****

いかがでしょう。  
アプリケーションを Dockernize してちょっと手間をかけてあげたことで、これだけのネットワーク構成を Docker コマンドだけで構築することができました。

# Ingress Load Balancing
Docker Swarm mode の、まだ説明していなかったもうひとつの機能。ロードバランスについて説明します。

## Nginx と Lua とリバースプロキシ
ロードバランスの説明の前に、先ほどのネットワークで「コンテナ間で通信するときのアドレス」について説明します。

図解するとこんな動きになってます。

![](images/ingress-01.png)

`http://portal.obic.net/marubatsu/message` という URL のリクエストを、 *proxy-public* の Nginx が受け取ると    
`marubatsu` というサブディレクトリの文字をひろって、 URL を書き換えます（rewrite）。  

書き換えられた URL、 `http://proxy-marubatsu/message` でリクエストを投げると、*proxy-marubatsu* に飛んでいきます。  
*proxy-marubatsu* は `message` の部分を拾って、 URL を `http://message-marubatsu` に書き換えます。

`http://message-marubatsu` でリクエストを投げると、 *message-marubatsu* のアプリケーションに届くという流れです。

ややこしいのですが、ポイントは

* Nginx で URL を書き換える
* サービス名に書き換えると、そのサービス(コンテナ)にリクエストが飛ぶ

という事です。

****

で、Nginx でやっている「URL の書き換え」のルールは、Nginx の設定ファイルである 「nginx.conf」 の中に書いています。  
nginx.conf は IIS でいうところの web.config 。

web.config は XML のファイルですが、 nginx.conf は独自フォーマットです。（JSON + Yaml っぽい）

Nginx の強みは、 nginx.conf によって複雑なルール（今回の URL の書き換えのようなもの）を定義しやすくしているところです。  
設定ファイルといいながら、その中で Lua という言語でスクリプトを埋め込むこともできます。

OBIC Portal では、 Nginx と Lua を駆使して、overlay network のネットワーク構成を実現しています。

実際のコードは、「動的リバースプロキシ」にしてるのでもうちょっと複雑。

![](images/nginx-01.png)

## 内部 DNS
もう一つ　「サービス名に書き換えると、そのサービス(コンテナ)にリクエストが飛ぶ」 というところ。

まず前提として、Docker コンテナは立ち上げると実は IP アドレスが内部的にふられます。  

そして、 overlay network を作った時に、そのネットワーク内にさりげなく内蔵 DNS サーバー(Embedded DNS Server)が立てられます。
その DNS サーバーが、 IP アドレスと「サービス名」を紐づけて保持しています。

なので、`http://marubatsu-message` のように「サービス名」を指定したアクセスは、 DNS によって `http://10.0.0.6` といった形で、コンテナの IP アドレスに置き換えてくれるのでコンテナ間の通信ができるようになっています。

![](images/ingress-02.png)

## ラウンドロビン
「サービス名を指定したアクセスは"コンテナ"の IP アドレス」と表現したのにはちゃんと意図があります。  

サービスは、 `--replicas` オプションで指定された数のコンテナを維持する、という話は「オーケストレーション」の文脈でしました。  

例えばメッセージのやり取りが頻繁に発生するので、コンテナをいっぱい立てて並列処理することになったとしましょう。


```
$ docker service create --name message-matubatsu --network marubatsu-net --replicas 3 portal/message
```

![](images/ingress-03.png)

すると、こんな風にコンテナが立ち上がり、IP アドレスがふられます。

ここに、先ほどと同じように「サービス名でアクセスするとどうなるか」というと

![](images/ingress-04.png)

こんな風に、「同じサービス・複数あるコンテナ」の IP アドレスを、一つずつ順番に返すようになります。  
いわゆる「ラウンドロビン」です。

`--replicas` を指定してコンテナの数を増やすだけ。 nginx.conf をいじることなく、ラウンドロビンによる負荷分散の仕組みができあがりました。

この Docker Swarm mode の機能を Ingress Load Balancing と呼びます。

# 結び
いかがだったでしょうか。  

これだけのネットワーク構成を、アプリケーションをちょっと Dockernize して、 Docker コマンドを実行するだけで実現できる。

これまでの記事でよく「簡単に」と表現してきましたが、その一端をご理解いただければ幸いです。  
<small>ここまで持っていくのにかなり頭を悩ませましたが… 特に Lua…</small>
