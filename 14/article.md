Windows とコンテナ
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の14日目の記事です。 

[13日目の記事](http://tkysva448/Hinata/item/de86be7b8a5d4e93889fe9f27d8cba83)では、 Docker の変遷について触れました。  

ずっと Linux の話をしてきましたが、今日は我らが Microsoft の話をしようと思います。

# Microsoft とコンテナ
Docker によってコンテナ型仮想化技術が脚光を浴び始めた当初、Microsoft は蚊帳の外にいました。  
Microsoft の主要 OS といえば、言うまでもなく Windows。  
Docker は Linux のコンテナを扱う技術。  
どうあがいても蚊帳の外ですね。

## Drawbridge
Microsoft もコンテナ仮想化技術を持っていなかったわけではありません。  
コンテナ型仮想化技術の元ネタ自体は古くからあるわけですし、 Microsoft の天才たちがその可能性を見過ごすわけがない。  

Microsoft Research という Microsoft の研究機間によって、2011 年には Windows 上で動くコンテナ仮想化技術が発表されています。

[Drawbridge](https://www.microsoft.com/en-us/research/project/drawbridge/) と呼ばれる技術です。

コンテナとは言いつつも、 今まで説明してきた Linux のコンテナをは少し違います。  
ごくごく小さな Windows である Library OS（カーネルを含む）でアプリケーションを包み込んで、その中でアプリケーションを実行する感じです。  
ホスト OS に依存したり、ホスト OS を汚すことなくアプリケーションを動かすことができ、また廃棄も簡単。そしてセキュア。  
そういうコンテナでした。

****

Drawbrdge は残念ながら大きく広まることなく、 Docker の流れに飲み込まれます。  
時期が悪かったのと、 「Windows だったから」としか言いようが…

## Docker 正式対応を発表
2014年10月、 Microsoft は Docker との合同発表で、 Docker の正式対応を発表しました。

### Azure での Docker サポート
Azure 上では、 Linux の仮想マシンを動かすことができます。  
当然ながら、 Linux 環境さえあれば Docker は使えるわけですが、より Docker を使いやすいように

* 管理ポータルに Docker Hub を統合
* 仮想マシンではなく、Docker のコンテナ実行環境のみをサービスとして提供する

といった方向性を示しました。

****

ちなみに、 Azure 上の仮想マシンですが、 現時点で作られているサーバーのうち 3 分の 1 が Linux です。  
[Microsoft by the Numbers](http://news.microsoft.com/bythenumbers/) というサイトで Microsoft の各種「数字」が公開されています。  

![](images/number-01.png)

なかなか面白いサイトなので、是非見てみてください。

Microsoft はすでに Windows 主体のビジネスから脱却していることがよくわかりますね。

### Docker クライアントの Windows サポート
コマンドラインツールの Docker クライアントは、もともとクロスプラットフォームで動くツールです。  
※Go 言語で書かれています。

その Docker クライアントを、 Windows でより使いやすくするためにサポートしていくと発表しました。

Docker は GitHub に公開されている OSS です。  
OSS に対して、企業として貢献することを宣言したのです。

ちなみに、Satya Nadella 氏が CEO に就任したのが2014年2月。  
8か月ですごい変わりっぷり。

### 次期 Windows Server へ Docker を統合
Azure への対応は、 Linux が動く時点で対応できそうだと想像もつきますが、合わせて **Windows Server** を Docker に対応させると発表しました。  
Linux のコンテナと似たような仕組みを Windows に用意して、さらに Docker で動かせるようにするということです。  
<small>ブラウザ戦争のころの MS とは全然違う。</small>

****

この時発表された「次期 Windows Server」が、今年（2016年）9月に正式リリースされた「Windows Server 2016」です。

## コンテナ戦争と Microsoft
Microsoft が Docker のサポートを発表してから 2か月後、コンテナ戦争が勃発。  
その翌年、標準化団体（OCI）の発足とともに戦争終結。

OCI には Microsoft も名を連ねています。

****

特にソースがあるわけではなく、私の感覚の話ですが  
コンテナ戦争は Microsoft にとって追い風になる出来事だったのではないかと想像しています。

![](images/win-01.png)

2014年10月の発表では、上図のようなイメージでした。  
Windows Server の中に Docker を入れて、コンテナを動かすイメージ。

それが、コンテナ標準化の流れによって

![](images/win-02.png)

標準化されたコンテナを動かすランタイムを Windows Server に組み込んでおけば、コンテナを操作するのは別に Docker じゃなくてもいい。  
という状況になりました。

## Windows Server 2016 リリース
「別に Docker じゃなくてもいい」という流れになりつつありましたが、やはりコンテナ界では Docker が一歩先んじています。  
というわけで、 Windows Server 2016 は素直に 「Docker でコンテナを扱う」形でリリースされました。

それにプラスして、 Windows Server を購入すると、 Docker の商用サポートを受けられるというおまけもついています。

****

こうして Microsoft はきちんと「コンテナ仮想化技術」を実装したわけですが、ややこしいことに2種類のコンテナを用意してきました。  
次の章で詳しく見ていきます。

# ふたつのコンテナ
Microsoft は Windows Server 2016 で 「Windows Server コンテナ」と 「Hyper-V コンテナ」という2種類のコンテナを用意しました。  
どちらのコンテナも Docker で扱えます。

それぞれ何が違うのか、 Linux のコンテナと何が違うのかを見ていきます。

## Windows Server コンテナ
Windows Server コンテナは、 Linux コンテナと同じ構造です。

今まで見てきた通り、 Linux が下図なら

![](images/linux-container-01.png)

Windows Server コンテナはこう

![](images/windowsserver-container-01.png)

一緒ですね。

****

「カーネルを共有する」という点も当然同じです。  
そして、Windows Server コンテナでは、 Linux のイメージからコンテナを作って動かすことはできません。  
だってカーネルが違うから。

![](images/windowsserver-container-02.png)

### Nano Server と Server Core
コンテナ上で動かすイメージとして、 Nano Server と Server Core という二つのイメージが用意されています。

どちらも Docker Hub 上に公開されていて、 `docker pull` で取得することができます。

* [Nano Server](https://hub.docker.com/r/microsoft/nanoserver/)
* [Server Core](https://hub.docker.com/r/microsoft/windowsservercore/)

Server Core は Windows Server 2008 ですでに実装されていたオプションです。  
「サーバーはサーバーとしての機能があればいい」
「デスクトップなんて飾りです」  
という精神から、コマンドラインで操作する UI だけ用意された Windows Server です。  
GUI はありませんが、サーバー機能はほぼ使えます。

Nano Server は、Server Core からさらに機能をそぎ落として軽量化したイメージです。  
イメージサイズは 500MB 程度。  
仮想環境を 100GB で用意してもらったのに、使えるのは 70GB程度（VS 入れたら 50GB）という Windows が 500MB。  
驚きの軽さ。  
代わりに、 .NET Framework は入ってません。 .NET Core が入っています。

これらのイメージを使って

* Server Core に SQL Server を入れて、データベースサーバー用のコンテナ
* Server Core に IIS を立てて、WEB サーバーやリバースプロキシサーバー用のコンテナ
* Nano Server に ASP.NET Core アプリを入れて、 WEB アプリ用のコンテナ

という、 Microsoft 一色のコンテナ環境を作ることができます。

****

ちなみに、 Windows Server コンテナで Linux のイメージを動かせないのと同様に  
Linux コンテナで、これらの Windows Server のイメージからコンテナを作って動かすことはできません。  
だってカーネルが違うから。

### Windows Server コンテナの使い方
Linux の時と何ら変わりません。  
`docker run` でコンテナを起動して、 `docker rm` でコンテナ削除。といったように、Linux の場合と全く同じ操作で Windows Server コンテナを扱うことができます。

## Hyper-V コンテナ
さて、もうひとつのコンテナの「Hyper-V コンテナ」についてです。  

説明より先に図解で表すとこんな感じ

![](images/hyper-v-container-01.png)

Docker の上に Hyper-V があるのは、今回はちょっと置いておきます。  
「コンテナ」と言いつつも、独自にカーネルを持ち、ホストとカーネルを共有しません。  

Linux のコンテナや、 Windows Server コンテナとはまるで実装が異なります。

「とても小さい仮想サーバーをコンテナっぽく扱っている」

といってもよさそうです。

ちなみに、コンテナに対するホストも、ハイパーバイザー上の仮想環境で動かすのが普通なので

![](images/hyper-v-container-02.png)

実際はこんな構図になります。

### どうしてこうなった？
端的に言うと、セキュリティ上の要求に答えるためです。

カーネルを共有する形のコンテナは、軽量さというメリットの引き換えに

* ホストのカーネルに脆弱性が見つかったときに危険にさらされる
* コンテナからホストを操作できる可能性がある

というデメリットを抱えます。

後者については、基本的に「余計なことをしなければ」そんなことは起こりません。  
「余計なことをすると」コンテナからホストを乗っ取るようなことも可能です。

詳しい説明は長くなるので省略させてもらい、記事紹介にとどめさせていただきます。  
[Dockerコンテナ内からホストマシンのルートを取る具体的な方法（あるいは/var/run/docker.sockを晒すことへの注意喚起） | 48JIGEN *Reloaded*](
http://rimuru.lunanet.gr.jp/notes/post/how-to-root-from-inside-container/)

****

サービスを提供する側が、サービスを受ける側（顧客）に  
「御社の環境はコンテナ上に展開し、マルチテナントで提供させてもらいます」  
と言った時  
「お隣さんのコンテナが乗っ取られたりしたらうちは大丈夫なの？コンテナは安全なの？」  
と返されたらどうしましょう。

「正しく運用しているので問題ありません」で納得してもらえればいいですが、「問題がない」ことの証明は不可能です。悪魔の証明です。  

カーネルを共有する形のコンテナではなく、個々のコンテナを完全に分離してセキュリティ的に強いコンテナも用意しておこう。  
そういった理由で作られたのが Hyper-V コンテナです。

### Hyper-V コンテナの作り方
Linux のコンテナや Windows Server コンテナとかなり作りが違いますが、 Hyper-V コンテナも Docker のコマンドラインから作ることができます。

```
$ docker run --isolation=hyperv
```

という風に、 --isolation オプションを指定するだけです。

作りは違っても、今まで通りの使い勝手でコンテナを作ることができるようになっています。

### カーネルを共有しないという事は？
理論上は、 Windows 上で Linux のイメージを動かすことができそうです。

ただ、今のところそういった情報は仕入れられていません。  
気が向いたときに調べてみようかなと思います。

# まとめ
* Windows Server コンテナ
    - カーネルを共有するタイプのコンテナ
        + Nano Server、 Server Core という Windows のイメージで作ったコンテナしか動かない
* Hyper-V コンテナ
    - コンテナが独自にカーネルを持っている
    - カーネルを共有するタイプに比べて分離度が高い
        + セキュリティの要求に答えるため
