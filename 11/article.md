Infrastructure as code の話
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の11日目の記事です。 

[10日目の記事](http://tkysva448/Hinata/item/????????)では、 アプリケーション開発と Docker の関わりについて触れました。  
今日と明日の記事では、「インフラ」の観点から Docker とコンテナについて触れてみます。  
今日は「構築」、明日は「運用」の二本立てでお送りします。

# Infrastructure as code
インフラを構築する。と一口に言っても多岐にわたりますが、ざっと OBIC7 の環境を整える上での例を挙げると

* ネットワークの設定
    - IP アドレスの割り振り
    - DNS
* OS の設定
    - CPU やメモリの設定
    - ディスク割り当て
    - ユーザーの作成
* ミドルウェアの設定
    - SQL Server
    - IIS
    - Remote Desktop Service
* セキュリティの設定
    - ファイアウォール
    - ウィルス対策ソフト
* バックアップタスクの設定
    - タスクスケジューラーの設定
    - テープバックアップのための機器設定

などなど。

「サービスを提供する」ことは、アプリケーションを開発すれば済むわけではありません。  
こういったインフラの構築を含めて「サービス」として成立します。

## 職人技の時代
インフラを構築する際に必要な知識は膨大です。  
OS、ネットワークなどの知識はもちろん、サードパーティーのミドルウェアの知識も必要だったり。

そういった知識は、実際に触ってみる・構築してみるといったフェーズで培われていきました。  
職人技の世界です。

もちろん、「構築マニュアル」を作って、学習コストを下げる努力もします。

****

かつてはそれでも良かったのですが…。  
ムーアの法則が終焉を迎えつつある昨今、一台のコンピューターで計算をさせるのではなく、分散させた大量のコンピューターで並列で処理するながれが主流になりました。  

要は管理するサーバーの台数がえらい増えたわけです。

仮想化技術も拍車をかけます。  
物理は少なくても、仮想を合計すると、何千・何万というサーバーを動かすような企業はざらに出てきました。

職人による手作業や技の伝授では、時間が足りません。  
それに、手作業が多いとそれだけミスも発生します。  

## コード化
職人技による手作業をなくすため、「コード化」が進みます。  
Windows だと「バッチファイル」、 Unix や Linux だと「シェルスクリプト」と呼ばれるスクリプトの利用です。

このバッチファイルやシェルスクリプトを実行すれば、インフラを自動で構築できる。  
そういう仕組みを用意し、「職人」でなくてもあるインフラ構築でき、かつ、大量のサーバーを構築するスキームを作り上げました。

## 構成管理ツール
バッチファイル、 PowerShell やシェルスクリプトで、インフラ構築を「コード化」することに成功しました。  
インフラをプログラミングする時代です。

ただ、バッチファイルやシェルスクリプトだと、プログラミングするには少し貧弱です。  

そこで、それらを拡張したり、より使いやすくするツールとして「インフラ構成管理ツール」が登場しました。

代表的なものとしては

* Chef
    - しぇふ
    - ruby で作られたツール
* Puppet
    - ぱぺっと
    - python で作られたツール
* Ansibel
    - あんしぶる
    - python で作られたツール

などがあります。

### Ansible
私自身、触ったことがないので深いところまでは説明できませんが、 Ansible を軽く例に挙げると

Ansible はサーバーの構成を Yaml と呼ばれる形式で定義します。  
Yaml（やむる）は、高機能な INI ファイルのようなものです。

[GitHub](https://github.com/ansible/ansible-examples) に上がっているサンプルを拝借するとこんな感じ

```yaml
# This playbook deploys the whole application stack in this site.

- name: apply common configuration to all nodes
  hosts: all
  remote_user: root

  roles:
    - common

- name: configure and deploy the webservers and application code
  hosts: webservers
  remote_user: root

  roles:
    - web

- name: deploy MySQL and configure the databases
  hosts: dbservers
  remote_user: root

  roles:
    - db
```

さらに掘り下げて、 Web サーバーを構築する部分では

```yaml
# These tasks install http and the php modules.

- name: Install http and php etc
  yum: name={{ item }} state=present
  with_items:
   - httpd
   - php
   - php-mysql
   - git
   - libsemanage-python
   - libselinux-python

- name: insert iptables rule for httpd
  lineinfile: dest=/etc/sysconfig/iptables create=yes state=present regexp="{{ httpd_port }}" insertafter="^:OUTPUT "
              line="-A INPUT -p tcp  --dport {{ httpd_port }} -j  ACCEPT"
  notify: restart iptables

- name: http service state
  service: name=httpd state=started enabled=yes

- name: Configure SELinux to allow httpd to connect to remote database
  seboolean: name=httpd_can_network_connect_db state=true persistent=yes
  when: sestatus.rc != 0
```

正直、不慣れだと何を書いているのかよく分からないところですが…。

シェルスクリプトで書くよりも、 Yaml を使うことで「宣言的」に書かれています。  
それにより可読性はかなり増しています。


### 冪等性
構成管理ツールはバッチファイルやシェルスクリプトの拡張だけでなく、「冪等性を担保する」という機能を有しています。

>数学において、冪等性（べきとうせい、英: idempotence　「巾等性」とも書くが読み方は同じ）は、大雑把に言って、ある操作を1回行っても複数回行っても結果が同じであることをいう概念である。まれに等冪（とうべき）とも。  
>[Wikipedia](https://ja.wikipedia.org/wiki/%E5%86%AA%E7%AD%89) より

何回実行しても、途中から実行しても、必ず同じ結果になることが担保される。  
手作業で実現するのは困難なことは想像に難くないですが、バッチファイルやシェルスクリプトでも実現するのは面倒です。  

途中から実行しても大丈夫となると、「SQL Server がインストールされてたら」や「IIS が有効になっていたら」といった if 文の嵐になります。  
それを構成管理ツールでは吸収してくれています。

### テスト
インフラ構築を「プログラミングする」と表現しました。  
プログラミングできるなら、ついでに実現したいのが「テスト」。

コードで書いたサーバーの構成が、本当に正しく構築されるかをテストすることも可能です。

Chef に対する ServerSpec、 Ansible に対する AnsibleSpec というツールがそれにあたります。  
テスト自体もコードで書きます。

****

C# でも MSTest や NUit を用いてテストコードを書いてユニットテストを実装することができます。  
それと同じことがインフラ構築においてもできるわけです。

テストファーストやテスト駆動開発（TDD）の文脈に触れると話がながくなるので割愛しますが、  
テストを書くことによって得られる安心感は大きいです。

## バージョン管理
バッチファイル、シェルスクリプト、構成管理ツールを使用するで、インフラがコード化されました。  
コード化されたということは、 TFS や Git などの「バージョン管理システム」に簡単に乗せられるようになります。

適切にバージョン管理されれば、不具合が起きた時に全く同じ状態のサーバーを構築することができ、不具合対応が容易になります。

変更した履歴も残せるので、「なぜ変更を加えたか」という文脈が残るわけです。  
かつては「構築した当人」、「変更した当人」しかわからなかった情報が、バージョン管理システムに乗ることでなくなります。  
属人化が排除されるわけですね。

アプリケーション開発と同じ。

## Infrastructure as code
「コード化する」、「構成管理ツールを使う」ことによって、インフラ構築がアプリケーション開発と同じような「プログラミング」の領域に至りました。

そして、アプリケーション開発で培われたフローである「テスト」や「バージョン管理」を組み込むことで、より健全にインフラを管理する事ができるようになります。

これらを総じて **Infrastructure as code** と呼びます。

****

かつては、Chef や Puppet、Ansible などの構成管理ツールを使えば「Infrastructure as code」という風潮がありましたが  
最近は、上記のように解釈するのが普通になってきています。

# Docker と Infrastructure as code
[8日目の記事](http://tkysva448/Hinata/item/????????)で触れたように、コンテナ自体は Docker が登場する以前から存在していた技術です。  
コンテナ自体は知っている人は便利に使っていた技術ですが、それこそまさに職人技、インフラエンジニアが独自に作成した秘伝のタレで使用されていました。

そこに Docker の登場です。

もうお気づきだと思いますが、Dockerfile が「コード化」「構成管理」、Docker のイメージが「冪等性」です。  
「テスト」という点は別のアプローチが必要ですが、総じて Docker そのものが Infrastructure as code を実現する要素を有していると言えます。

****

コンテナ自体が昔からあったこと、 Infrastructure as code の下地ができていた、という背景もあって  
Docker の登場自体はインフラエンジニアにはすんなりと受け入れられたわけです。

# まとめ
* Infrastructure as code とは
    - コード化
        - バッチファイル
        - シェルスクリプト
    - 構成管理ツール
        - Chef、Puppet、Ansible
        - 冪等性
        - テスト
    - バージョン管理

Docker がインフラ構築の問題を解決したというわけではなく  
インフラ構築のソリューションをコンテナの世界にも持ち込んだのが Docker  
ということです。
