コンテナの現在 ～Docker 以外のツール紹介～
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の16日目の記事です。 

[15日目の記事](http://tkysva448/Hinata/item/36e6912ec06746c9bc38a505dc39801a)では、最新の Docker が持っているクラスター化、ネットワーキング、オーケストレーションの機能について触れました。

今日は、そんな Docker 以外の、ほかのツールを紹介したいと思います。  
といってもこれは Docker アドベントカレンダーなので、かるーくです。  

# CoreOS
[13日の記事](http://tkysva448/Hinata/item/de86be7b8a5d4e93889fe9f27d8cba83)で触れたコンテナ戦争を引き起こした張本人。  
こあおーえす。

[https://coreos.com/](https://coreos.com/)

![](images/coreos-01.png)

コンテナのランタイムとして、 **rkt** を用意しています。

****

Docker の Swarm mode と同じように、クラスター構成を組むことができます。  
同じようにというか、クラスター構成の仕組みは CoreOS のほうが先発。

etcd （えとせでぃー）という KVS （Key-Value Store） を使用してクラスターを実現しています。  
etcd は複数の OS 間で設定を共有する仕組みです。

****

OCI にて Docker とともにコンテナの標準化に携わり、現在ではちゃんと CoreOS 上で Docker を動かすことをサポートしています。

# Kubernetes
Google が作成した OSS のコンテナクラスター管理ツールです。  
「くーばねいてす」と読みます。

[http://kubernetes.io/](http://kubernetes.io/)  

![](images/kubernetes-01.png)

このツールは、 Docker と組み合わせて使う場合が多いです。  

コンテナ自体は Docker で扱う。  
クラスター構成を Kubernetes で実現する。  

という感じです。

****

そもそも Google は、 10年前から「コンテナ」を使ってサービスを展開していました。  

2014年のスライドですが、 [こちら](https://speakerdeck.com/jbeda/containers-at-scale) によれば、2004年からコンテナ仮想化を使い始めて  
2014年時点で「すべての」サービスがコンテナ上で動いているとのこと。

We start over 2 billion containers per week.  
一週間に 20億のコンテナを起動している。

というわけで、 Docker の登場するはるか昔から Google はコンテナを使いこなしていたわけです。  
私たちも知らず知らずのうちに、コンテナ仮想化で構築されたサービスを触り続けていたんですね。

Docker の登場によりコンテナが大衆化されたことを受けて、 Google が長年培ってきたコンテナの運用ノウハウを OSS として公開したのが Kubernetes です。

# Apache Mesos
クラスター構成において、コンテナを効率的に配分する仕組みです。  
「あぱっち めそす」と読みます。

[http://mesos.apache.org/](http://mesos.apache.org/)  

![](images/mesos-01.png)

これも Docker と組み合わせて使うタイプです。  
クラスター構成を組んだ時、コンテナをどのホストに立ち上げるかというところを自動でやってくれます。  
ホストの状態に応じて。

Mesos だけだと、「コンテナを起動させ続ける」という機能がないので、あわせて [Marathon](https://mesosphere.github.io/marathon/) （まらそん）という仕組みを使ったりします。

# Docker との関わり
だいぶ大雑把な説明ですが  
コンテナ、特にクラスター構成にまつわるツール類を紹介しました。

Docker が登場したてのころ、「コンテナは使える。でもどうやって運用する？」という問題がありました。  
その問題に対するソリューションとして、これらのツールが使われたわけです。

Docker + クラスター構成のツール

という組み合わせでの運用です。

****

では、昨日の記事で紹介した Docker 自体の Swarm mode という機能は、これらとどう関わるかというと

競合関係

にあります。

Docker 以外のツールで解決してきた問題領域を、 Docker 単独で解決できるようにした。  
それが Docker 1.12 というバージョンです。

コンテナの標準化に伴い、 Docker じゃなくても（例えば rkt）コンテナを扱える状況になったため  
新たなブランディングとして Docker が「運用」の領域に踏み込んだのです。

# まとめ
* CoreOS
    - etcd を使ってクラスター構成を組む
    - rkt というランタイムでコンテナを管理する
    - CoreOS 上で Docker を動かすこともできる
* Kubernetes
    - Google の作成した OSS
        - Google は 10年以上前からコンテナを使っていた
    - クラスター構成を組んだホストでコンテナを管理するためのツール
* Apache Mesos
    - クラスター構成を組んだホストの上にコンテナを効率的に配置するツール
    - コンテナを起動し続けるためには Marathon という別のツールが必要

****

今日の記事はこれぐらいで。  
というのも、偉そうに紹介しましたが、これらのツール使ったことないので…

いよいよ明後日の記事から、 OBIC Portal というプロジェクトと Docker のかかわりについて書いていく予定ですが  
なぜこれらのツールを（いまのところ）使わない選択をしたのか、というあたりもちょっと触れようと思います。

****

これらのツールに興味を持たれた方は、ぜひ調べてみてください。  
そして私に詳細を教えてくださいm(__)m