Docker でデータベースサーバーを立てる
====

この記事は Advent Calendar 2016 Extra Docker の4日目の記事です。 

3日目の記事では、 ASP.NET Core のアプリケーションを Docker を使ってコンテナ上で動かすことができました。  
VS 2015 ではほとんど意識することはありませんでしたが。。。

ここまで、Nginx (Web サーバー)、 ASP.NET Core (アプリケーション) を動かしてきたので、今回は **データベースサーバー** をコンテナ上で動かしてみます。

# PostgreSQL
データベースサーバーには、 PostgreSQL を使ってみます。  
オープンソースの RDBMS で、SQL Server と同じ位置付けにあるデータベースサーバーと思ってください。
「ぽすとぐれすきゅーえる」と読みます。大抵は省略して「ぽすとぐれす」「ぽすぐれ」と呼んでいます。

## docker pull
Nginx の時と同じように、今回も Docker Hub からイメージを取得してきます。  
Nginx の時に省略した箇所について説明を追加させていただきます。

```sh
$ docker pull postgres:9.6
```

`docker pull postgres` でも問題ないのですが、 `:9.6` と後ろにつけました。  
これは「タグ」と呼ばれ、同じイメージ名に対して、バージョン分けする時に使われます。

Docker Hub の PostgreSQL のページを見てみると  
[https://hub.docker.com/_/postgres/](https://hub.docker.com/_/postgres/)

![](images/postgres-01.png)

同じ PostgreSQL のイメージでも、複数のバージョンが用意されていることがわかります。

タグをつけなかった場合は、 `latest` タグのついたイメージが使われます。  

## docker run
コンテナを立ち上げます。

```sh
$ docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=mydb --name my_postgres postgres:9.6
```

`-e` は、コンテナの中で使われる **環境変数** を指定するオプションです。  
使い方は、 [Docker Hub の PostgreSQL のページ](https://hub.docker.com/_/postgres/)に書かれているので、それに従います。

![](images/postgres-02.png)

`POSTGRES_USER` で、データベースサーバーにつなぐ時のユーザー  
`POSTGRES_PASSWORD` で、そのユーザーのパスワード  
`POSTGRES_DB` で、コンテナを立ち上げた時に、最初に用意するデータベース名

を指定しています。

****

例のごとく `docker ps` してみると

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
a3803bc3490e        postgres:9.6        "/docker-entrypoint.s"   2 minutes ago       Up 2 minutes        0.0.0.0:5432->5432/tcp   my_postgres
```

コンテナが立ち上がっています。

# コンテナ上の PostgreSQL につないでみる
Nginx や ASP.NET の時は、ブラウザがありましたが、データベースサーバーとなるとそれ用のクライアントが必要になります。  
なので、今回は pgweb という Web クライアントを使ってみようと思います。

pgweb は Go 言語で書かれたオープンソースのアプリケーションで、 [GitHub にソースが公開されています](https://github.com/sosedoff/pgweb)。  

PC 上で動かせるアプリケーションですが、これもついでにコンテナ上で動かしてみます。  
Go 言語と ASP.NET Core の違いはありますが、 4日目の記事と同じノリだと思ってください。

****

[https://github.com/sosedoff/pgweb/wiki/Docker](https://github.com/sosedoff/pgweb/wiki/Docker)

ここの情報を元に、コマンドを実行します。

```sh
$ docker pull sosedoff/pgweb
$ docker run -d -p 8081:8081 --link my_postgres -e DATABASE_URL=postgresql://postgres:password@my_postgres:5432/mydb?sslmode=disable --name pgweb sosedoff/pgweb
```

ブラウザで `http://localhost:8081` につないでみます。

![](images/pgweb-01.png)

pgweb が起動しています。 
pgweb は Web ブラウザで PostgreSQL を操作できるツールです。  
「SQL Server Management Studio の PostgreSQL 版で、さらに Web ブラウザで使うことができる」  
こんな感じでしょうか。

![](images/pgweb-02.png)

## --link オプション
`docker run` で指定した `--link` というオプションですが、これは**ほかのコンテナにつなぐ**ためのオプションです。  
pgweb のコンテナは、 PostgreSQL のコンテナとは別に作られています。

```sh
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
baf6bf9fbb9c        sosedoff/pgweb      "/usr/bin/pgweb --bin"   15 minutes ago      Up 15 minutes       0.0.0.0:8081->8081/tcp   pgweb
a3803bc3490e        postgres:9.6        "/docker-entrypoint.s"   45 minutes ago      Up 45 minutes       0.0.0.0:5432->5432/tcp   my_postgres
```

他のコンテナにつなぐ時に、コンテナの名前 （ここでは my_postgres ）を指定することで繋がるようにする。それが `--link` オプションです。

`-e` で `DATABASE_URL=postgresql://postgres:password@my_postgres:5432/mydb?sslmode=disable` と指定しています。  
`postgresql://` から始まる文字列が、PostgreSQL版の**接続文字列**です。  
`{postgresql://{ユーザー名}:{パスワード}@{サーバー名}:{ポート番号}/{データベース名}` という順になっています。  
`--link` オプションを指定したことで、「サーバー名」のところに「コンテナ名」を指定することができるようになりました。

# まとめ
* データベースサーバーもコンテナ上で動かすことができる
* `-e` でコンテナで使用する環境変数を指定できる
* コンテナから他のコンテナにつなげることができる
