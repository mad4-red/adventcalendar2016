OBIC Portal と Docker ～インフラ～
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の23日目の記事です。 

昨日まではアプリケーション寄りの話だったので、少しインフラ・ミドルウェアにフォーカスを当ててみます。

# Swarm mode と相性の悪い「データ永続化」
OBIC Portal のサービスを支える仕組みとして、 Docker Swarm mode を採用しました。  
Swarm mode のおかげで、ホストを気にしなくても Docker がよしなにコンテナを配置してくれるわけですが  
データベースなど、「データを永続化」する機能は残念ながらうまく適用できません。

## Volume の仕組み
[5日目の記事](http://tkysva448/Hinata/item/22b0e9ab20884fc58073ce5fbe680758) で PostgreSQL のデータを永続化する仕組みを説明しました。
`docker volume create` というコマンドで *Volume* というものを作成し、そこにデータを保存するというものです。

この *Volume* の仕組みをもう少し詳細に説明します。

5日目の記事と同様に、 *Volume* を作ります。

```sh
$ docker volume create --name my_postgres_data
```

*Volume* の構造を見るために、 `inspect` サブコマンドを打ちます。  

```sh
$ docker volume inspect my_postgres_data
```

※ `inspect` サブコマンドは、 *Volume* だけじゃなく、いろいろなところで詳しく知りたいときに打つと便利なコマンドです。 `docker inspect コンテナ名` とか。

こういった結果が出力されます。

```
[
    {
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/my_postgres_data/_data",
        "Name": "my_postgres_data",
        "Options": {},
        "Scope": "local"
    }
]
```

注目してほしいのが、 *Mountpoint* の部分です。  
`/var/lib/docker/volumes/my_postgres_data/_data` となっているところ、実はこれ**ホストのパス**（Windows 風にいうとフォルダ）です。

*Volume* という機能は

* ホストのパスに名前を付ける
* `docker run -v` で *Volume* を指定すると、コンテナからはそのパスが自身のパスのように見える

という機能です。

*Volume* が「ホストのパス」という存在がある以上、「ホストによらずコンテナを立ち上げる」という Swarm mode では使えません。  
つまり、 *Volume* を使ってデータ永続化をするデータベースなどは Swarm mode では使えません。

# Infrastructure as Code としての Docker
Swarm mode で使えないからといって、 Docker を使わない、とはなりません。  
データベースなどに対しても、 Docker を使ってコンテナ化するメリットがあります。

その最たるところが *Infrastructure as Code* です。

機能出てきた *Jenkins* を例に見てみましょう。  
Jenkins もビルドの定義やログを保持するので、データ永続化が必要な仕組みです。

これが OBIC Portal で使っている Jenkins の Dockerfile です。

```Dockerfile
FROM jenkins:2.19.1

USER root

# Install docker client
RUN apt-get update \
    && apt-get install -y sudo curl \
    && rm -rf /var/lib/apt/lists/* \
    && echo "jenkins ALL=NOPASSWD: ALL" >> /etc/sudoers \
    && curl -fsSLO https://get.docker.com/builds/Linux/x86_64/docker-1.12.2.tgz \
    && tar --strip-components=1 -xvzf docker-1.12.2.tgz -C /usr/local/bin \
    && rm -f docker-1.12.2.tgz

# Install dotnet
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        liblttng-ust0 \
        libunwind8 \
    && rm -rf /var/lib/apt/lists/* \
    && curl -sSL -o dotnet.tar.gz https://go.microsoft.com/fwlink/?LinkID=827530 \
    && mkdir -p /usr/share/dotnet \
    && tar -zxf dotnet.tar.gz -C /usr/share/dotnet \
    && rm dotnet.tar.gz \
    && ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet

ENV NUGET_LOCAL_PACKAGES /var/data/nuget/packages
RUN mkdir -p /var/data/nuget/packages \
    && chown -R jenkins /var/data/nuget/packages
VOLUME /var/data/nuget/packages

# Install node.js and npm
RUN apt-get update \
    && apt-get install -y nodejs npm \
    && rm -rf /var/lib/apt/lists/* \
    && npm cache clean \
    && npm install -g n \
    && n stable \
    && ln -sf /usr/local/bin/node /usr/bin/node \
    && apt-get purge -y nodejs npm

# Install build utils
RUN npm install -g \
        bower \
        gulp \
    && npm cache clean

# Create backup VOLUME
RUN mkdir -p /var/data/backup \
    && chown -R jenkins /var/data/backup
VOLUME /var/data/backup

# Install postgres-client
RUN apt-get update \
    && apt-get install -y postgresql-client \
    && rm -rf /var/lib/apt/lists/*

USER jenkins

COPY docker-keys /docker-keys
ENV DOCKER_CERT_PATH /docker-keys
```

細かいところは置いておきますが、 **Jenkins でビルド**するために、`dotnet` や `node.js` を Jenkins のコンテナにインストールする命令を Dockerfile に書いています。

****

OBIC7 の開発用サーバーを運用したり、ほかの人から引き継いだりすると、こういう事ってありませんか？  

「このサーバー、 Java や Python がインストールされてるけど、これ何なの？」

何かしらのミドルウェアをインストールする際に必要だったのか何なのか、なぞのコンポーネントが入っていたり…  

なぜそれらがインストールされているか  
アンインストールしたらどういった影響があるのか  
最新バージョンにアップデートしたらどういった影響があるのか

それらを読み解くのはなかなかの労力です。

Jenkins の例を見ると、 Dockerfile を用いて「意図をもってインストールしたコンポーネントやミドルウェア」が一目でわかるようになっています。  
これにプラスして、 TFS などのバージョン管理システムに変更履歴を適切に残すことで、「インストールした文脈」も残すことができます。

根本は「適切な運用」であり、Docker を使う事自体が銀の弾丸ではありませんが  
Docker を用いた Infrastructure as Code によって、**意味の分からないサーバー** が生まれづらくなることはお分かりいただけたでしょうか？

OBIC Portal では、 Jenkins をはじめ、 PostgreSQL 、 Docker Registry 、 ログを収集する仕組み（明日の記事）などなどを Docker コンテナとすることで Infrastructure as Code を実践しています。

# おまけ： Docker Compose

OBIC Portal の Jenkins のコンテナを立ち上げる時に打つコマンドですが

```
$ docker run -d -p 9003:8000 -p 50000:50000 -e TZ=Asia/Tokyo --name jenkins -v data:/var/jenkins_home jenkins
```

こんな感じです（実際はもっと長い）。  
こんな長ったらしいコマンドをいちいち打つのは非常に面倒。  
それに、これだけ長いと途中で間違えるかもしれない。

というわけで、ここもコード化して TFS に乗っけます。  
一番手っ取り早いのが、シェルスクリプト(バッチファイル)ですが、 Docker はシェルスクリプトよりもより「構造的」な記法でコンテナの立ち上げ方を記述する仕組みを用意しています。  

それが *Docker Compose* です。

`docker-compose.yml` というファイルに Yaml 記法で記述して

```
$ docker-compose up -d
```

というコマンドをたたくと、上に書いたような `docker run` コマンドが走るという仕組みです。

実際に Jenkins で使っている `docker-compose.yml` はこんな感じ

```yaml
version: "2"
services:
    jenkins:
        build: jenkins
        ports:
            - 9003:8080
            - 50000:50000
        volumes: 
            - data:/var/jenkins_home
            - nuget_data:/var/data/nuget/packages
            - backup_data:/var/data/backup
        environment: 
            - TZ=Asia/Tokyo
            - DOCKER_HOST=${DOCKER_HOST}
            - DOCKER_TLS_VERIFY=${DOCKER_TLS_VERIFY}
        container_name: jenkins
        restart: always
        hostname: jenkins
        logging:
            driver: fluentd
            options:
                fluentd-address: tkyvr05038.tky.obic.co.jp:24224
                tag: docker.backend.jenkins
                fluentd-async-connect: "true"
volumes:
    data:
        external:
            name: jenkins_data
    nuget_data:
        external:
            name: jenkins_nuget_data
    backup_data:
        external:
            name: jenkins_backup_data
networks:
    default:
        external:
            name: backend_bridge_net
```

これも一つの 「Infrastructure as Code」 です。

****

実は、 Docker Compose は Docker Swarm mode では使えません。  
<small>なので、最初のほうの記事で省略してからいままで放置してたのですが</small>

Swarm mode では `distributed application bundles` 略して `DAB` というファイルで実現するのですが、これはまだまだ機能不足。  
これから先のバージョンで強化されていくものと思われます。  
※ OBIC Portal では Swarm mode の部分はシェルスクリプトでやってます

「まだ覚えなきゃいけない要素があるのか」とは思わないでくださいね。  
「今までできてなかったことと、その状態を継続することによるコスト」に比べたら、これらを覚えることで発生するコストは微々たるものです。  
それに加えて、 Chef や Ansible などの構成管理ツールを使うのではなく、 Docker の世界で完結するのは、 Docker を使う大きなメリットでもあります。

# まとめ
* Docker Swarm mode では、データ永続化が必要なコンテナは動かせない
* データ永続化が必要な機能でも、 Docker を使うことで Infrastructure as Code が実践できる
* Docker Compose を使うことで、 Docker コマンドの内容を docker-compose.yml ファイルに構造的に記述することができる
    - Docker Compose は Docker Swarm mode では使えない