この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の20日目の記事です。 

今日の記事では、 OBIC Portal プロジェクトのアプリケーション開発環境についてご紹介します。

# 開発言語と開発環境
[18日の記事](http://tkysva448/Hinata/item/3e400b8252914cc8b3e7edf9cd7bb4ba) で触れましたが、  
まず、 Linux を使うことが決まりました。

そこで問題になるのが開発言語。  
われらが C# は Windows の .NET Framework 向けのプログラミング言語です。

C# は ISO で標準化されていて、 Mono プロジェクトによって Linux で動くのはもちろんなのですが  
それでも、最新の .NET Framework で使える機能が未実装だったり、つらみがあります。

C# は Windows。

なので Linux を選択する以上、 Ruby などのほかの言語に手を広げるべきかと一時期考えてはいました。

そこに折よく登場したのが .NET Core。
クロスプラットフォームで動く .NET 。  
Linux で C# が書ける。

というわけで、以前の記事でも触れましたが、 ASP.NET Core、 C# で開発しています。  

****

.NET Core & C# ということで、 OBIC で今まで培ってきたノウハウを大部分は継承することができます。

開発環境は Visual Studio 2015。 ASP.NET Core の開発ができるので。  
バージョン管理（VCS）には VS と親和性の高い TFS を用いています。

Visual Studio  
![vs-01.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/840a9918d0af44ca9091c77c4c9169c6.png)

TFS  
![tfs-01.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/ec109345d53043c19e7fd95b672e62d5.png)

****

アプリケーション開発は、 Windows 上で、 Visual Studio を使っておこないます。  
デバッグやビルドも Windows 上。  

実際の運用環境である Linux との差異は、 .NET Core がよしなに吸収してくれます。  
なので、これまでと何ら変わらない環境で開発できています。

# 開発環境と Docker
では、 Docker はというと…  
[初日の記事](http://tkysva448/Hinata/item/36a8134c22b2490d8ecbf70e0f35070b) に書いた通り、Windows の仮想環境に Docker はインストールできません。

というわけで、 Docker for Windows や Docker Toolbox は使わず、 Linux サーバーに Docker をインストールして、 Windows の Docker クライアントからそこにつなぎます。

Linux は CentOS を採用。  
採用した理由は特段ありません。  
しいて言うなら、将来外部のサポートを受けることにしたとき、おそらく第一候補にあがるのが Red Hat。  
Red Hat のサポートしている Linux ディストリビューション、 Red Hat Enterprise Linux (RHEL)。
RHEL と CentOS に互換性があるから。といった点です。

ざっくり図にするとこんな感じ

![dev-01.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/1ab8c0fb92ad49b98488c989487b018c.png)

## 問題点
ただ、この構成だと残念な問題があって  
[3日目の記事](http://tkysva448/Hinata/item/d74d0c7c0b524ac88b9ee74e1e51c98f)でとりあげた 「Visual Studio Tools for Docker」 がちゃんと動きません。

「Visual Studio Tools for Docker」は、 Docker for Windows ないしは Docker Toolbox で、 「Visual Studio のある Windows 上に構築された仮想 Linux サーバーを使う」ことを前提にしています。  
Docker for Windows や Docker Toolbox は、 Linux サーバーと Windows のフォルダを共有する仕組みなっています。  
そうすることで、 Windows 上で Docker を動かしているように見せかける仕組み。

今回の構成は、 Windows と Linux が別々に存在するので、「Visual Studio Tools for Docker」 は無し。  
Visual Studio Code のくだりで書いたように、 Docker とのやりとりはコマンドライン操作で行います。

# プロジェクトとコンテナ
上に乗せた TFS のスクリーンショット。  
フォルダ一つ一つが小さなサービスとして独立したソリューションで構成されてます。

![dev-02.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/49b6a0d2b6024e5eb66d639f56a40168.png)

OBIC7 のクラサバに例えると、各画面の exe のような分け方です。  
もっとも、「受注入力」や「受注照会」のような小分けにされた形ではなく、「受注管理」というレベルで一つにまとまっています。

実際の運用環境では、これら一つ一つのサービスをコンテナの中で実行させます。

****

プロジェクトの中身はというと  
とくに変わった様子もなく、普通の ASP.NET のプロジェクト。

![tfs-03.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/46af1d8dc89740debe96f930d2844cf0.png)

Dockerfile などの Docker 関連ファイルを加わえているぐらいです。

# ビルドとデプロイ
というわけで、プロジェクトの構成は「コンテナだから」といった要素は Dockerfile ぐらいです。  

ここまでの内容は「まぁ、そーなのかー」ぐらいで受け取ってもらえれば OK です。
コンテナで動かすといっても特に気負う必要もなく、普通の WEB アプリケーション開発とほぼ変わりません。

ちょっと様子が変わるとすれば、ビルドとデプロイ。

OBIC7 を含め、普通に「アプリをサーバーで動かす」手順は

![dev-03.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/1ba3c7ef34634107b0b6f0d9e96af06a.png)

ですが、 Docker を使うとこうなります。

![dev-04.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/eaed00f21232442189bef276ef5443d9.png)

「ビルド」は二つに分かれます。  

まずは C# からアプリを作るビルド。  
ASP.NET Core アプリの場合は、 `dotnet publish` で、WEB アプリケーションが作成されます。  
※ Visual Studio で「発行」や「公開」と表現されている処理。

次に、Docker イメージのビルド。  
ビルドされたアプリのファイル群を、 Dockerfile の定義に従って Docker イメージの中に閉じ込めます。

コンテナを使う構成にしたことで、一段階手順が増えたような感じです。  

## ひとくふう
このビルドを毎回コマンドラインでやっていると手間です。  
なので、ビルド用のシェルスクリプト（バッチファイル）を作ります。

![script-01.png](http://192.168.3.175/bloqs/Hinata/hiraki_s/5d5cfb4ba2954f3cbf2c00a576e25d0a.png)

コマンドラインで操作できる ＝ 簡単にシェルスクリプトに組み込める

Docker コマンドはもちろん、 .NET Core のコマンドラインツールも [GNU スタイル](http://www.gnu.org/prep/standards/html_node/Command_002dLine-Interfaces.html)] になので、非常に扱いやすくなっています。  

****

余談ですが、 Windows のコマンドラインと、 Linux や Unix のコマンドラインでは少し文化が違います。  
参考：[コマンドラインプログラムにおける引数、オプションなどの標準仕様 | プログラマーズ雑記帳](http://yohshiy.blog.fc2.com/blog-entry-260.html)

Microsoft も最近は **GNU スタイル** と呼ばれる規格でコマンドラインツールを作ることが多くなってきました。

# ここまでまとめ
記事を書きながら改めて思ったんですが…  
「Docker を使うから何か開発が変わるか」というと、別にそうでもなかったという…

たいして変わらないね！
なんかちょっと手間が増えてない？

ぐらいの感想を持って頂ければ、明日以降の記事が映えるはずということで。

# Dockernize
とはいえ、「コンテナで扱いやすいように」ちょっとした変更を加えるところはあります。

たとえば「データベース接続文字列」。

ASP.NET Core アプリケーションでは、標準で *application.json* という JSON ファイルに設定値を書き込むようになっています。

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=task;"
  },
}
```

ASP.NET Core アプリの Startup で JSON を読み込み、アプリを初期化します。


```csharp
public class {
    private IConfigurationRoot _config;

    public Startup(IHostingEnvironment env) {
        var builder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json");
        _config = builder.Build();
    }

    public void ConfigureServices(IServiceCollection services) {
        var connectionString = _config.GetConnectionString("DefaultConnection");
        services.AddDbContext<MyDbContext>(options => options.UseNpgsql(connectionString));
    }
}
```

****

いざコンテナを立ち上げる時に、 *application.json* というファイルに設定値が盛り込まれていると厄介です。  
このままだと、「データベースの接続先ごとに、 Docker イメージを用意する」ことになります。非常に手間です。

そこで使うのが「環境変数」。

[4日目の記事](http://tkysva448/Hinata/item/5b329a9bdecc429aaafbfba50050c9d8)で出てきた。

```sh
$ docker run -e HOGE=fuga
```

`-e` オプションで指定するやつです。  
これによって、コンテナ起動時に、そのコンテナの中だけで有効な設定値を盛り込むことができます。

****

ASP.NET Core には、環境変数を利用するライブラリが用意がされているため

```csharp
public class {
    private IConfigurationRoot _config;

    public Startup(IHostingEnvironment env) {
        var builder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables(); // 環境変数の読み込み
        _config = builder.Build();
    }

    public void ConfigureServices(IServiceCollection services) {
        var connectionString = _config["DB_CONNECTION"]; // 環境変数から接続文字列を取得
        services.AddDbContext<MyDbContext>(options => options.UseNpgsql(connectionString));
    }
}
```

このようなコードに書き換えてやります。

コンテナを起動するときは

```
$ docker run -e "DB_CONNECTION=User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=task;" portal/task
```

こんな感じで環境変数経由で設定値を渡します。

****

このように、コンテナ、とくに Docker コンテナで扱いやすいようにアプリケーションを変更することを Dockernize （どっかーないず）と呼んだりします。

# まとめ
* コンテナを使っても、アプリケーション開発はあまり変わらない
* Docker コンテナで扱いやすいように、 Dockernize する部分も多少ある

個々のアプリケーションは、それぞれ新しいことに挑戦しているので、そこに触れようかなとも思ったんですが  
ほら、私、インフラ周り担当なもので。
