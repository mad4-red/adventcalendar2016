# Linux とコンテナ型仮想化

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の8日目の記事です。 

[7日目の記事](http://tkysva448/Hinata/item/?????)では Docker から離れて「サーバー仮想化技術」ということで、ホスト OS 型とハイパーバイザー型について触れました。  
今回は、もう一つの仮想化技術、「コンテナ型仮想化」について触れます。  
今日も Docker には触れずに行きます。  
「コンテナ」という単語は使いますが、「Docker」という単語は頭の中から追いやって読んでください。

# Linux
コンテナについて触れる前に…

すでに何度か Linux という単語を出してきました。  
ここで軽く「Linux とは何か」という点に触れておこうと思います。

といっても私もたいして詳しくないので、大部分は [Google 先生](https://www.google.co.jp/search?client=ubuntu&channel=fs&q=linux&ie=utf-8&oe=utf-8&hl=ja#channel=fs&hl=ja&q=Linux%E3%81%A8%E3%81%AF)にお任せしますが。

****

Linux は、 Windows や macOS と並んでよく登場する OS の一つです。

一口に Linux といっても、 Ubuntu、Debian、CentOS、Red Hat Enterprise Linux (RHEL) など、いろいろと名前が付いたものがたくさんあり、「Linux 系」などと括られたりします。  
これらはいったい何なのか。

## Unix と Linux
Linux のほかに Unix というものも存在します。  
名前が似ているので、「*nix」といったようにまとめられたりもします。

Unix は 1969 年に誕生した古い OS です。ベル研究所が作成しました。  
誕生後、非常にシェアを伸ばしましたが、ライセンス契約などで徐々に自由度が失われていきます。

自由に使えなくなった Unix をやめて、オープンソースで 1 から Unix っぽいものが作られました。
リーナス（Linus Benedict Torvalds）という当時大学生の天才によって。

![](images/linus-01.jpg)

Linux は Unix を模して作られており、使用感（といっても CUI ですが）もよく似ています。（ただし、中身は全く別物）

Unix を使用していたユーザーは、自由に使えるオープンソースの Linux にこぞって移行していきます。  
Linux は瞬く間にシェアを拡大していきました。

****

もうちょっと知りたい人は

[【初心者向け】Linuxの歴史解説！ OS誕生からLINUXへ](http://eng-entrance.com/linux_birth)

[３分間で人に説明できるようになるUnixとLinuxの違い](http://eng-entrance.com/unix_linux)

****

コンテナを語るうえで両方の名前が出てくるので触れさせてもらいました。

## Linux とは
上述した通り、一口に Linux といっても、 Ubuntu、Debian、CentOS、Red Hat Enterprise Linux など色々な名前が出てきます。

リーナスの作り上げた Linux とそれらはどんな関係があるのか。

### Linux カーネル
単純に Linux と表現した場合は「Linux カーネル」を指します。

カーネルとは

![](images/kernel.png)

ユーザーの使用するアプリケーションが、ハードウェアを直に操作しなくて済む、あるいは直に操作させないようにするための仲介を行う仕組みです。  
いわば **OS の中核**のようなものです。

アプリケーションとハードウェアの仲介をするシステムコールのほか、プロセス管理・メモリ管理する仕組みをそなえたもの、それがカーネルです。  
そして、それがリーナスの作ったものです。

****

カーネル自体は Linux に限らず Windows、 macOS も持っています。  
Windows の場合は「NT カーネル」。

### Linux ディストリビューション
普通のひとがコンピューターを使おうと思っても、カーネルだけじゃ何もできません。  

例えば「デスクトップ」というアプリケーション。  
これがないと、真っ黒な画面で文字の入出力だけで何とかしないといけません。  

Linux カーネルと、利用用途に合わせたアプリケーションをパッケージングして、最初から使いやすくしたものが Linux ディストリビューションです。  
※ディストリビューション＝分布 · 流通

Ubuntu、Debian、CentOS、Red Hat Enterprise Linux などはそれにあたります。

### いったんまとめ
**カーネル**という OS の中核にあたる仕組みがある。  
とりあえず、これがポイントです。

# コンテナ
昨日の記事で、少し歴史について語りました。  
そこからさらに過去に遡ります。

<small>と言っても、私も入社どころか生まれてない頃の話だったりするので、ネットで聞きかじった話です。</small>

物理一つに OS 一つで、物理を大量に購入していた時代よりもっと前。  
物理環境が非常に高価で、大量に仕入れることもままならない時代。

コンピューターに色々やらせたいのに、サーバーは一台しかない状況。  

****
例え話ですが  
OBIC7ex を統括サーバーに入れて、何百社もの環境を会社コードを分けて入れていた状況と似ています。

一つのサーバーでうまくやりくりしていましたが、大きな問題がありました。  

* パッチは全社に同時に全て当たってしまう
* 会社ごとのカスタマイズは当てられない

実際の客先環境（オンプレミス）では、その会社で使う分の環境しかありません。  
パッチを当てたり、カスタマイズを入れたりすると、確実に客際環境とはズレが生じます。

「会社ごと業務ごと」にパッチ当てられたらいいのに。。。って思いません？

今の OBIC7 でも似たようなことありますよね。  
本番環境とテスト環境で会社を分けたいけれど、アプリは同じだからテスト納品が出来ない。  
ASecurity から分けないと…  
でもユーザー登録とか二重になるし、DBも増えるから、A系は共有でいいんだけどなぁ…

##　Unix の chroot と Jail

昔のコンピューターも同じような状況に陥りました。  
高価なサーバーを 1 台購入して、みんなで使いまわしたい。  

けれど、Aさんが処理したい計算で使うライブラリは、Bさんの計算処理で使うライブラリと共存できない。

その解決方法として、 chroot という機能が Unix に組み込まれました。  
1979年のことです。

chroot によって、一つサーバーの中で「Aさん用の環境」「Bさん用の環境」と分け、**互いに干渉しない**状況を作ることができるようになりました。

単純に「ユーザーアカウント」といった話ではありません。  
**互いに干渉しない**環境というのがミソです。  

****
正確ではないことは重々承知で、ばっくりとイメージを語ると

**OBIC7 で、会社ごと業務ごとに環境を切り離す仕組みができたので、カスタマイズやらパッチやらを、会社ごと業務ごとに適用できる**

こんな感じです。便利！
****

ハードが安価になり一つの環境を小分けにする必要が薄れて行きますが、「知っている人は便利に使える機能」として chroot は生き続けます。  
そして 21 年の時を経て、 Jail という機能に進化していきます。

chroot は単純に環境を切り離すだけでしたが、 Jail によって完全に独立した OS が動く「ような」状態を作れるようになりました。  
つまり「仮想化」です。そしてこれが、コンテナの原型となります。

細かいことはいいんです…  
もともとは 1979 年という昔から存在する技術だったということだけ伝われば…

## Linux の cgroups と LXC
Unix の Jail が便利、というこで Linux にもその機能が移植されました。  
cgroups という機能です。（厳密には Jail とは異なるのですが）

cgroups を使いつつ、さらに仮想的に OS を簡単に動かせるようにと、 Linux コンテナ（LXC）が誕生しました。

****

Unix の Jail、 Linux の cgroups、LXC は、chroot のころと変わらず「知っている人は便利に使える機能」という位置づけです。  
コンテナ型仮想化技術は古くから存在する機能をブラッシュアップすることで成立してきましたが、ホスト OS 型やハイパーバイザー型の仮想化が隆盛を迎えていたので、ひっそりと使われるに留まっていました。  

それがなぜ、今になって取り上げられるようになったのかは、10日目からの記事で詳しく取り上げていきます。

## コンテナ型仮想化の特徴
コンテナ型仮想化の大きな特徴は「カーネルを共有する」ことにあります。  

![](images/container-01.png)

図のように、コンテナ上の OS はカーネルを持っていません。

ホスト OS 型やハイパーバイザー型の場合は

![](images/hostos-01.png)

![](images/hypervisor-01.png)

ゲスト OS はカーネルを含んでいます。  

****

「最低限、環境を切り分ける」という事を起点に発展してきた技術です。

![](images/container-011.png)

ハードウェアとのやり取りをするカーネルは、ホスト OS のものを使いまわす形で問題ないわけです。  

### メリット
カーネルを持たないことにより、コンテナ上のイメージは非常に**軽量**になります。  
「軽量であること」が最大のメリットになります。  
どうメリットになるかは、10日目の記事で。

### デメリット
カーネルを共有するということは、そのカーネルで動く OS しか、コンテナ上に乗せることはできません。

Linux の場合は Linux ディストリビューションという形で複数の OS が存在するので

![](images/container-03.png)

こんな構成にすることはできますが  
Linux のコンテナ上で Windows が動くことは絶対にありえません。  

ホスト OS 型やハイパーバイザー型の場合はホストとゲストの OS が違っても問題ありません。  
Hyper-V の上で Linux は動きますし、 macOS に入れた VMWare Fusion の上で Windows や Linux は動きます。

それと比較すると、 動く OS が限られるのはデメリットといえるでしょう。

# まとめ
* OS にはカーネルという、ハードウェアとやりとりをする中核部分がある
* コンテナの歴史は古い
* コンテナはカーネルを共有する
    - Linux コンテナの上では Linux しか動かない
* コンテナ仮想化の場合、ゲスト OS はカーネルを持つ必要がないので**軽量**になる

