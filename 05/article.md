コンテナのライフサイクルとデータの永続化
====

この記事は Advent Calendar 2016 Extra Docker の5日目の記事です。 

4日目の記事では、 データベースサーバーをコンテナ上で動かしました。  
実は、一つ重要な内容をすっ飛ばしました。

**データベースサーバーをコンテナ上で動かしたけど、データは保存されない。**

データベースサーバーとしては致命的ですね。。。

まずは、**データが保存されない状態**というのを実際に見てみます。  
そして、データを保存する（永続化する）方法について説明します。

# データが保存されないとは
昨日と同じように、 PostgreSQL と pgweb を使って、データベースサーバーと操作する環境を作ります。

```sh
$ docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=mydb --name my_postgres postgres:9.6
$ docker run -d -p 8081:8081 --link my_postgres -e DATABASE_URL=postgresql://postgres:password@my_postgres:5432/mydb?sslmode=disable --name pgweb sosedoff/pgweb
```

`http://localhost:8081` にアクセスして、テーブル作成・データを INSERT します。

```sql
create table my_table (
  id int,
  value text
);

insert into my_table values (1, 'データ投入したよ！');

select * from my_table;
```

![](images/postgres-01.png)

データが INSERT されて、 SELECT で結果セットが返ってきました。

## コンテナを削除する

この状態で `docker ps` すると

```sh
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
a1c6b7d4ce27        sosedoff/pgweb      "/usr/bin/pgweb --bin"   2 days ago          Up 6 minutes        0.0.0.0:8081->8081/tcp   pgweb
a37abdd135b5        postgres:9.6        "/docker-entrypoint.s"   2 days ago          Up 6 minutes        0.0.0.0:5432->5432/tcp   my_postgres
```

PostgreSQL と pgweb のコンテナが立ち上がっています。

これらを削除します。

```sh
$ docker stop my_postgres
$ docker rm my_postgres
$ docker stop pgweb
$ docker rm pgweb
```

```sh
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
```

なくなりました。

## もう一度 PostgreSQL のコンテナを立ち上げる
最初と同じコマンドで、もう一度 PostgreSQL と pgweb のコンテナを立ち上げます

```sh
$ docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=mydb --name my_postgres postgres:9.6
$ docker run -d -p 8081:8081 --link my_postgres -e DATABASE_URL=postgresql://postgres:password@my_postgres:5432/mydb?sslmode=disable --name pgweb sosedoff/pgweb
```

で `http://localhost:8081` につないでみます。

![](images/postgres-02.png)

SQL 文自体は**ブラウザが記憶していたため**残っちゃっていますが、肝心のデータはどうなっているかというと…

SELECT 文だけ流してみます。

![](images/postgres-03.png)

```
ERROR: pq: relation "my_table" does not exist
```

「`my_table` なんてテーブルはない」というエラーになってしまいました。

# コンテナのライフサイクル
PostgreSQL と pgweb のコンテナを作り直した状態で、 `docker ps` してみます。

```sh
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
35556d9029ab        sosedoff/pgweb      "/usr/bin/pgweb --bin"   2 days ago          Up 6 minutes        0.0.0.0:8081->8081/tcp   pgweb
314dda46fc09        postgres:9.6        "/docker-entrypoint.s"   2 days ago          Up 7 minutes        0.0.0.0:5432->5432/tcp   my_postgres
```

最初にコンテナを作った時と作り直した後で、 `CONTAINER ID` が変わっています。

* PostgreSQL: `a1c6b7d4ce27` -> `314dda46fc09`
* pgweb: `a37abdd135b5` -> `35556d9029ab`

同じイメージ、同じコマンドでコンテナを立ち上げましたが、立ち上がったコンテナは **ID の異なる全くの別物のコンテナ**なのです。

****

コンテナは、 `run` で作成されて `rm` で削除されるまでの間しか生存しません。  
たとえ同じイメージからコンテナを作成しても、作成されたコンテナは唯一無二。一期一会の存在です。

「テーブルを作ってデータを挿入した」のは、あくまで `ID:a1c6b7d4ce27` のコンテナに対してであって、 `ID:314dda46fc09` は知る由もありません。  
なので、コンテナを作り直したあとに SELECT しても、エラーが返ってきたわけです。

「メモリ上のデータが揮発する」というイメージに似ています。

# データの永続化
このままでは、データベースサーバーとしての意味がありません。  
もちろん、データを残す方法を Docker は用意しています。

## ボリューム
コンテナとは別の概念で**ボリューム**というものがあります。  

細かいことは置いておいて、ボリュームを作ります。

```sh
$ docker volume create --name my_postgres_data
```

ボリュームができました。

作成されたボリュームは `docker volume ls` で確認できます。

```sh
$ docker volume ls
DRIVER              VOLUME NAME
local               my_postgres_data
```

## -v オプション
いったん、コンテナを削除して、もう一度作り直します。

```sh
$ docker rm -f my_postgres
$ docker rm -f pgweb
```

※ `rm` に `-f` オプションを付けると、 `stop` せずに無理やりコンテナを削除することができます。

PostgreSQL のコンテナを作るときに、オプションをさらに付け足します。

```sh
$ docker run -d -p 5432:5432 -v my_postgres_data:/var/lib/postgresql/data -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=mydb --name my_postgres postgres:9.6
```

`-v my_postgres_data:/var/lib/postgresql/data` というオプションを追加しました。

これは、 「`my_postgres_data` というボリュームを、コンテナ内の `/var/lib/postgresql/data` にマッピングする」という意味になります。  
`/var/lib/postgresql/data` には、 SQL Server でいうところの `mdf` や `ldf` が入っていると思ってください。

pgweb も立ち上げて、最初と同じくテーブルを作成し、データを INSERT します。

![](images/postgres-04.png)

この状態で `docker ps` して、コンテナの ID を確かめておきます。

```sh
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
46dbde996f28        sosedoff/pgweb      "/usr/bin/pgweb --bin"   2 days ago          Up 2 seconds        0.0.0.0:8081->8081/tcp   pgweb
db467fe72b1e        postgres:9.6        "/docker-entrypoint.s"   2 days ago          Up About a minute   0.0.0.0:5432->5432/tcp   my_postgres
```

## コンテナを削除してもう一度作る
```sh
$ docker rm -f my_postgres
$ docker rm -f pgweb
$ docker run -d -p 5432:5432 -v my_postgres_data:/var/lib/postgresql/data -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=mydb --name my_postgres postgres:9.6
$ docker run -d -p 8081:8081 --link my_postgres -e DATABASE_URL=postgresql://postgres:password@my_postgres:5432/mydb?sslmode=disable --name pgweb sosedoff/pgweb
```

```sh
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
a7cd3747ce46        sosedoff/pgweb      "/usr/bin/pgweb --bin"   2 days ago          Up 1 seconds        0.0.0.0:8081->8081/tcp   pgweb
2f0cbc760e82        postgres:9.6        "/docker-entrypoint.s"   2 days ago          Up 12 seconds       0.0.0.0:5432->5432/tcp   my_postgres
```

PostgreSQL のコンテナ ID は `db467fe72b1e` から `2f0cbc760e82` に変わりました。  
先ほどとは違うコンテナです。

pgweb からデータを確認してみると

![](images/postgres-05.png)

データは残ってました。

# ボリューム
ボリュームは、コンテナとは切り離されたライフサイクルで生きています。  
ボリュームをあらかじめ作っておくことで、コンテナの作成、削除にかかわらずデータを保管しておくことができます。

ボリューム自体の削除は

```sh
$ docker volume rm my_postgres_data
```

と、 `volume rm` コマンドで実行することができます。

データを永続化は、この「ボリューム」という機能を使用することで解決しました。

# まとめ
* コンテナの生存期間は `run` から `rm` まで
    - 同じイメージからコンテナを作っても ID の異なる全く別物
* コンテナを再作成するとデータは消えてなくなる
* データを残したい（永続化したい）場合はボリュームを作成する
* ボリュームはコンテナとは切り離されたライフサイクル