Docker 開発環境のインストールと基本コマンド
====

この記事は Advent Calendar 2016 Extra Docker の１日目の記事です。  

いよいよ Advent Calendar が始まりました。  
今日から25日間、 Docker を中心に、コンテナ・ソフトウェアアーキテクチャ・インフラストラクチャ・オープンソースソフトウェアなどについて語っていこうと思います。  

****

さて、いきなり Docker と言われても

* とっくに触ってる
* 聞いたことはあるけれど、どんなものか知らない
* 聞いたことない

と、色々な方がいらっしゃると思います。  
なので、全25回を通して

* Docker で何ができるのか
* Docker とは何なのか
* Docker (コンテナ) が解決する問題領域とは
* 実際に Docker を使って何をやっていくか（やっているか）

という順序で記事を構成していきます。  
すでに Docker を触っている方にとっては、最初の方の記事は既知の内容と思いますので、読み飛ばしていただいてかまいません。  
各記事のタイトルはすでに[カレンダー](todo)の方に記載しているので、そちらをご参照ください。

後半の方のタイトルに書いてある通り、 OBIC Portal というプロジェクトと Docker との関わりもできるだけ書いていくつもりです。  


****

では１日目の本題に入ります。  

まずは、 Docker をインストールして軽く使ってみましょう。  
習うより慣れろ、です。  

ただ、あらかじめ謝っておきます。  

会社の PC 、仮想環境にはインストールできません。  
習うより慣れろと言いながらこの状況、本当に申し訳ないのですが。。。  
なぜインストールできないかという種明かしも追々していきますので、まずはご自宅のPC等でおためしください。

※この記事も、自宅の MacBook・VAIO を駆使して書いております。。。  
※12/17までは、外部に持ち出しても問題ない機密度で記事を書くつもりです。

# Docker をインストールする
## Windows の場合
まずは Windows の場合。

### 事前チェック
Docker をインストールする前に、クライアント Hyper-V が使用可能かどうか確認します。  
クライアント Hyper-V は、 Windows 8 から使用可能になった機能です。  
お使いの PC が Windows 7 以前の場合は、「Docker ToolBox のインストール」にお進みください。

コントロールパネル > プログラムと機能 > Windows機能の有効化または無効化 を開きます。  
![](images/hyper-v-01.png)

Hyper-V プラットフォームにチェックをつけて「OK」。  
再起動が求められたら、再起動してください。


クライアント Hyper-V が使用できないエディション、CPU、ハードだと、チェックがつけられないようになっています。   
![](images/hyper-v-02.png)

Hyper-V が使えるはずなのに上記のようになっている場合は、 BIOS の設定を確認してみてください。  
[参考：ASCII.jp：Windows 8で搭載された仮想環境「Hyper-V」を有効にする (2/2)｜Windows Info](http://ascii.jp/elem/000/000/913/913933/index-2.html)

### Docker for Windows のインストール
クライアント Hyper-V を有効にできたら、 Docker for Windows をインストールしましょう。

[こちら](https://docs.docker.com/docker-for-windows/)から、インストーラをダウンロードします。  
![](images/docker-for-win-site.png)  
※*Stable*ってついてる方

ダウンロードしたら `msi` を実行。

![](images/docker-for-win-01.png)

![](images/docker-for-win-02.png)

![](images/docker-for-win-03.png)

インストールしたバージョンより新しいのがあるっぽいので Update  
![](images/docker-for-win-04.png)

![](images/docker-for-win-05.png)

これで完了です。

### Docker Toolbox のインストール
Docker for Windows をインストールできない場合は、Docker Toolbox をインストールします。

[こちら](https://github.com/docker/toolbox/releases/tag/v1.12.3)からインストーラーをダウンロードして実行。

![](images/docker-toolbox-01.png)

…  
…  
…  

インストール手順をペタペタ貼りたかったのですが、実は問題があって  
Docker for Windows と Docker Toolbox は共存できません。

なので

ちょっと古いバージョンですが、 Qiita に良い記事があったのでそれをお借りすることでお茶を濁させてもらいます。

[Docker Toolboxのインストール：Windows編](http://qiita.com/maemori/items/52b1639fba4b1e68fccd)

## Mac の場合
Mac には、 Docker for mac、 Docker ToolBox の共存が可能です。  
Windows と違って、そう障壁はないはずなので、 Docker for Mac のインストールだけ説明します。

****

Docker for Mac を[こちら](https://docs.docker.com/docker-for-mac/)からダウンロード。

![](images/docker-for-mac-site.png)  
※*Stable*ってついてる方

dmg ファイルを展開して
![](images/docker-for-mac-01.png)  

![](images/docker-for-mac-02.png)  

![](images/docker-for-mac-03.png)  

途中、パスワードを求められたら入力すれば

![](images/docker-for-mac-05.png)

インストール完了です。

# Docker を使ってみる
インストールが完了したら、次は Docker を軽く使ってみましょう。  
Windows の場合はコマンドプロンプト、Mac の場合はターミナルを立ち上げてください。  
もちろん、コマンドプロンプトが使いづらいので他のコンソールをインストールしている方はそれを使って頂いて構いません。

## Docker コマンドを叩いてみる
コマンドプロンプト、ターミナルを立ち上げたら、とりあえず以下のコマンドを打って見てください。

```sh
$ docker version
```
※ 「コマンドを入力する」ということを示すために、慣例的に頭に `$` をつけています。実際に入力するときは `docker version` だけで構いません。

すると

```
Client:
 Version:      1.12.3
 API version:  1.24
 Go version:   go1.6.3
 Git commit:   6b644ec
 Built:        Wed Oct 26 23:26:11 2016
 OS/Arch:      darwin/amd64

Server:
 Version:      1.12.3
 API version:  1.24
 Go version:   go1.6.3
 Git commit:   6b644ec
 Built:        Wed Oct 26 23:26:11 2016
 OS/Arch:      linux/amd64
```

こんな風に出力されれば、ちゃんと Docker がインストールされて動いている状態です。

## docker pull、docker run
ここからは、 Docker の基本的なコマンドを説明しながら、Docker で何ができるのかを見ていきます。  
コマンドというと、黒い画面で何やら小難しいことをするイメージを持つ人もいるかもしれませんが  
恐れることなかれ、余計な画面がない分、シンプルで簡単な UI なんです。

****

では、とりあえず以下のコマンドを打ってみましょう。

```sh
$ docker pull nginx
```

すると

```
Using default tag: latest
latest: Pulling from library/nginx
386a066cd84a: Pull complete 
7bdb4b002d7f: Pull complete 
49b006ddea70: Pull complete 
Digest: sha256:9038d5645fa5fcca445d12e1b8979c87f46ca42cfb17beb1e5e093785991a639
Status: Downloaded newer image for nginx:latest
```

何かをダウンロードして、展開する動きが出力されます。

次は

```sh
$ docker run -d -p 8080:80 nginx
```

と入力します。

ブラウザを立ち上げて、 `http://localhost:8080` にアクセスしましょう。

![](images/nginx-01.png)

何やらWebのページが表示されています。

### 何をやったか
Nginx とは、オープンソースの Web サーバーです。（「えんじんえっくす」と読みます）  
IIS と同じようなものと考えてください。

Windows サーバーに IIS を立てるには、「コントロールパネル > プログラムと機能 > Windows の機能の有効化または無効化」から、「World Wide Web サービス」を有効にします。

その一連の作業と同じようなことを、 `docker pull`、 `docker run` でやっているような感じです。

****
`docker pull` では、[Docker Hub](https://hub.docker.com/) というサイトから、 Nginx の**イメージ**をダウンロードしてきます。  

`docker run` では、ダウンロードしてきた **イメージ** を **コンテナ** として起動します。  

**イメージ** や **コンテナ** という言葉については、のちの記事で詳しく書いていきます。  
ここでは、「コマンドをたたくだけで、**簡単に** Web サーバーが立ち上がった」という事実に着目しておいてください。

## docer ps
次は、このコマンドを打ってみます。
```sh
$ docker ps
```

そうすると

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                           NAMES
084c0ce68ab0        nginx               "nginx -g 'daemon off"   6 minutes ago       Up 6 minutes        443/tcp, 0.0.0.0:8080->80/tcp   jolly_stallman
```

今動いている **コンテナ** の一覧が表示されます。  
`NAMES` に `jolly_stallman` と入っているのは、 Docker がコンテナにランダムでつけた名前です。

もう一つ、今度は自分で名前をつけてコンテナを作ってみましょう。

```
$ docker run -d --name my_nginx -p 9000:80 nginx 
```

`docker ps` をすると

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                           NAMES
084c0ce68ab0        nginx               "nginx -g 'daemon off"   7 minutes ago       Up 7 minutes        443/tcp, 0.0.0.0:8080->80/tcp   jolly_stallman
9ff13777cd61        nginx               "nginx -g 'daemon off"   8 seconds ago       Up 7 seconds        443/tcp, 0.0.0.0:9000->80/tcp   my_nginx
```

増えました。

`docker run` する時に `--name` というオプション引数を指定することで、自分で名前をつけることも可能です。  

あと、 `-p` は、「どのポート番号でコンテナに繋がるようにするか」を指定するオプションです。  
最初に作った `jolly_stallman` に `8080` を割り当てたので、もう `8080` は使えません。  
なので二つ目は `9000` を割り当てました。  
※もう少し詳しい話は二日目に

## docker stop、docker start
```sh
$ docker stop jolly_stallman
```

`stop` コマンドでコンテナを止められます。  
そのままですね。

コンテナが止まったので、 `http://localhost:8080` にアクセスすると

![](images/nginx-02.png)

```sh
$ docker start jolly_stallman
```

`start` コマンドで再度立ち上げることができます。

## docker rm
作ったコンテナを、止めるだけではなく完全に削除します。

```sh
$ docker stop jolly_stallman
$ docker rm jolly_stallman
```

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                           NAMES
9ff13777cd61        nginx               "nginx -g 'daemon off"   7 minutes ago       Up 7 minutes        443/tcp, 0.0.0.0:9000->80/tcp   my_nginx
```

`jolly_stallman` は消えてなくなりました。

****
ここまでは **コンテナ** に関するコマンドです。  

次は **イメージ** に関するコマンド

## docker images、docker rmi

```sh
$ docker images
```

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx               latest              05a60462f8ba        2 weeks ago         181.5 MB
```

`images` コマンドを打つと、 **イメージ** の一覧が出力されます。  
このイメージは `docker pull` した際にダウンロードされたものです。

```sh
$ docker rmi nginx
```

`SIZE` という列があることからわかる通り、イメージの分だけ容量を食います。  
※正確には違うのですが、今はざっくりそう捉えてください。

なので、使わなくなったイメージは削除しましょう。

`rmi` コマンドを打つと、ダウンロードしたイメージを削除できます。

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
```

****

基本コマンドはとりあえずこれぐらいで。  

まだ他にもあるし、オプション引数にも色々あるので、興味が出てきたら [Qiita](http://qiita.com/tags/docker) や、[公式ドキュメント](https://docs.docker.com/engine/reference/commandline/) を見てみてください。

# まとめ
* Docker はインストーラーを使って **簡単に** インストールできる
* Docker は **コマンドライン** で操作する
* **イメージ** と **コンテナ** というものがある

これぐらいを押さえてもらえれば幸いです。

## 蛇足
* インストール簡単だし、始めるしきいは低いですよ
    - ハマりどころは沢山あるけど…
* コマンドラインは楽しいですよ
    - 黒い画面（コマンドプロンプト）を使うの苦手な人もいるかもですが、慣れると「システム触ってる！」って感じがして楽しくなってきますよ
    - Windows 10 からは `ctrl+v` 使えるようになりましたし
    - 慣れると「もう GUI 要らない」という麻疹にかかります

## 蛇足2
* 「自宅では Ubuntu 使ってるんだけど、インストールの説明してくれないの？」
    - Ubuntu 使ってる方なら、多分インストールの説明いらないような気が…
        - もちろん他の Linux ディストリビューションも
    - Windows だけならまだしも、 Mac でのインストールも書いたことは、この先の展開の布石だったりして