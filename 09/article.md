# Docker とは

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の9日目の記事です。 

[8日目の記事](http://tkysva448/Hinata/item/????????)では、 Linux とコンテナについて説明しました。  
これで、 Docker とは何かについて語る下地ができました。

初日から完全に放置していた「Docker とは何か」という内容にいよいよ触れようと思います。

# Docker を構成する要素
## コンテナを簡単に作成する機能
Linux では cgroups という機能を使うことで、コンテナを作ることができます。  
ただ、直に cgroups を扱うことは非常に面倒です。

面倒な操作をラップして、簡単に扱えるようにしたのが Linux コンテナ（LXC）です。

Docker は、この LXC をラップして、後述の機能を付加しつつ、さらに**簡単に**コンテナを作成する仕組みとして誕生しました。

`docker run` という「コマンドを打つだけ」で、コンテナを作成できるわけです。

****

docker は LXC のラッパーとして登場しましたが、その後 LXC とは別物に成長します。  
この辺りの話は 12/13 日の記事で触れようと思います。

## コンテナに展開するイメージを作成する機能
LXC 単体は、「コンテナという環境を作る」という機能を持っているだけです。  
ただ、箱を作るだけ。

Docker はそこに、「コンテナ内に展開するイメージを作成する」機能を付け加えました。
それが[2日目の記事](http://tkysva448/Hinata/item/????????)で紹介した Dockerfile です。

この「イメージを定義するファイル」があることで、「何回でも同じ環境を**簡単に**作る」ことができるようになりました。

****

「何回でも同じ環境を作る」という行為は、インフラ管理の面で重要な要素になります。  
その話は 12/11 の記事で触れます。

## イメージを保管するリポジトリとエコシステム
Dockerfile は `FROM` で「元となるイメージ」を指定します。  
「元になるイメージは」はリポジトリと呼ばれる保管庫に置かれています。  

それが [Docker Hub](https://hub.docker.com/) です。

Docker はイメージを**簡単に**取得する仕組みを提供してくれています。

****
[2日目の記事]()で使用した Nginx を例に上げると

```Dockerfile
FROM nginx

COPY index.html /wwwroot/
COPY nginx.conf /etc/nginx/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```

`FROM nginx` とすることで Nginx のイメージを元にしています。

さらに Nginx の元は何かというと

![](images/nginx-01.png)

[Docker Hub](https://hub.docker.com/_/nginx/) のページから [GithHub](https://github.com/nginxinc/docker-nginx/blob/de8822d8d91ff0802989bc0a12f8ab55596a513c/mainline/jessie/Dockerfile) へ辿ることができます。

Nginx の Dockerfile では

```Dockerfile
FROM debian:jessie
```

と書かれています。

Debian という OS の jessie というバージョンをもとにしています。

Debian は Debian で [Docker Hub](https://hub.docker.com/_/debian/) にイメージがあります。

![](images/debian-01.png)

Nginx のイメージは、 Debian ではなく Alpine という OS を使ったものもあります。  
※Alpine は必要ないものを削りに削ったすごく軽量な OS です。

![](images/nginx-02.png)

Alpine は Alpine で、 [Docker Hub](https://hub.docker.com/_/alpine/) にイメージがあります。

![](images/alpine-01.png)

本来なら  
Debian は [Debian のサイト](https://www.debian.org/) から ISO を取得して  
Alpine は [Alpine のサイト](https://www.alpinelinux.org/) から ISO を取得して  
というようにちょっと面倒です。

イメージを一か所に集めることで、取得を簡単にしてくれています。  

### docker push
Docker Hub には、自分で作ったイメージをアップロードすることも可能です。

```
$ docker push
```

というコマンドでアップロードできます。

自分が作ったものはもちろんのこと、ほかの人がより便利なイメージを作成して `push` してくれていれば、それを利用することもできます。

Docker のイメージをアップロードしたり、ほかの人が作ったイメージダウンロードしたり  
さらにそこに、より便利な機能を追加してアップロードしたり
さらにほかの人が、それを加工したり

そういった**エコシステム**自体を Docker は提供してくれています。

### プライベートリポジトリ
Docker Hub は、アップロード (`push`) したイメージは基本全世界に公開されます。  
企業などで Docker を使用していて、自社内だけでイメージを保管・展開したい場合などは、有料オプションの「プライベートリポジトリ」を使用することになります。

これが Docker の開発元であり、Docker Hub の運営をしている Docker 社の収益源のひとつです。

****

また、オンプレミスで Docker Hub のようなリポジトリを作成することもできます。  
「Docker Registry」というアプリケーションを使用することで、社内のオンプレミスの環境でも無料でイメージの保管庫を作れます。  
※もちろん、自社のハード資産などをつかうので厳密には無料ではないですが。  
※Docker Hub のようにブラウザで見れるような UI はありません。Rest API のみです。

OBIC Portal プロジェクトでは、 Docker Registry を使って社内でイメージを管理しています。

12/23 の記事あたりで触れるつもりです。

## Dockerfile とイメージの差分管理機能
Dockerfile を使ってイメージを作成し、Docker Hub のようなリポジトリに保管するわけですが、そのイメージ自体が一工夫されています。

それは「差分管理」です。

再び Nginx を例にとって、見てみます。

久々に Docker コマンドを、2日目の Dockerfile を使用しつつ打ってみます。 
※コンテナやイメージは `rm`、`rmi`で消して、まっさらにしています。 

```sh
$ docker pull debian:jessie
$ docker pull nginx
$ docker images
```

最後の `docker images` で出力される結果は

```
REPOSITORY          TAG                 IMAGE ID            CREATED                  SIZE
nginx               latest              abf312888d13        9 days ago               182 MB
debian              jessie              73e72bf822ca        4 weeks ago              123 MB
```

`debian:jessie` と `nginx` は Docker Hub からダウンロードしてローカルにキャッシュしている状態です。  
注目したいのは `SIZE` の列。

これはそのものずばり「イメージ」のサイズですが、 `debian:jessie` と `nginx` の合計 `305 MB` が、ローカルで使われているわけではありません。  
`nginx` は `debian:jessie` を元にしているので、 `nginx` は実質 `59 MB` だけです。

さらに、 `docker build` します。

```
$ docker build -t my_nginx .
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED                  SIZE
my_nginx            latest              95a2759c4465        Less than a second ago   182 MB
nginx               latest              abf312888d13        9 days ago               182 MB
debian              jessie              73e72bf822ca        4 weeks ago              123 MB
```

`my_nginx` では 2 つファイルを足しただけなので、もはや MB のサイズでは表に出てきませんね。

これが差分管理です。

![](images/image-01.png)

****

「差分」の単位は実際はさらに細かく、「命令」単位です。

```Dockerfile
FROM nginx

COPY index.html /wwwroot/
COPY nginx.conf /etc/nginx/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```

この `COPY` や `EXPOSE` の一つ一つの結果が、 tar ボール（zip ファイルのようなもの）にまとめられて管理されています。

![](images/image-02.png)

ここで、 Dockerfile に命令をひとつ追加してみます。


```Dockerfile
FROM nginx

COPY index.html /wwwroot/
COPY nginx.conf /etc/nginx/

EXPOSE 80

RUN echo "hello world"

CMD ["nginx", "-g", "daemon off;"]
```

この状態で `docker build` して出来上がるイメージは

![](images/image-03.png)

こんな感じになります。

`EXPOSE 80` までは一度 `build` されたときに tar ボールにまとまっているので再利用。  
`RUN echo "hello world"` 移行を新しく tar ボールにまとめる。

という動きになります。  

すごく細かい単位で差分管理されているということです。

****

差分で管理しているため、イメージがあまりストレージを使用しないというメリットがあります。  
そして何よりも、一度キャッシュしておけば、 `docker pull` や `docker run` が**非常に速く**なります。

## クライアントツール
Docker 本体は Linux のコンテナを扱うので、**Linux 上で動きます**。
ただし、 Docker を操作するのは別に Linux からである必要はありません。

Docker は「クライアントツール」がある「クラサバ」システムなのです。

実は今まで打ってきた Docker コマンドは、サーバー上で動いている Docker を遠隔地から操作する「クライアントツール」の機能だったのです。

例えるならば、 SQL Server と SQL Server Management Studio のような感じ関係。

****

初日にさりげなく出てきたコマンド、 `docker version` を思い出してください。

```
Client:
 Version:      1.12.3
 API version:  1.24
 Go version:   go1.6.3
 Git commit:   6b644ec
 Built:        Wed Oct 26 23:26:11 2016
 OS/Arch:      darwin/amd64

Server:
 Version:      1.12.3
 API version:  1.24
 Go version:   go1.6.3
 Git commit:   6b644ec
 Built:        Wed Oct 26 23:26:11 2016
 OS/Arch:      linux/amd64
```

`Client` と `Server` 、それぞれのバージョンが表示されてるでしょう？

Docker は本体は Linux サーバーのアプリケーションですが、それを操るクライアントがあります。  
そして、クライアントは Windows、 Macでも動く、クロスプラットフォームのツールです。

サーバー上の Docker と Docker クライアントは TCP で通信しています。

****

ここまで説明してきた

* コンテナを簡単に作成する機能
* コンテナに展開するイメージを作成する機能
* イメージを保管するリポジトリとエコシステム
* Dockerfile とイメージの差分管理機能
* クライアントツール

をひっくるめて、 Docker というものなのです。

# Windows や Mac にインストールした Docker
ここまでの内容で、[6日目の記事]()の

>Linux の環境がないと Docker は動かせません。
>
>じゃあ、 Windows や Mac にインストールした Docker は何なのかという話ですが  
>実はあれ、「Windows や Mac の上に Linux の環境を作って、そこで Docker を動かしていた」んです。

の説明がつきます。

まず、 Linux の環境がないとどうしようもありません。

そこで、Windows の場合は Hyper-V、 Mac の場合は xhyve というハイパーバイザーを使用して、仮想環境に Linux を用意します。  
Hyper-V を有効にしたのはそのためです。

![](images/hyper-v-01.png)

ハイパーバイザーを使えない場合は、 ホスト OS 型の VirtualBox を使います。

仮想に立てた Linux で Docker の本体を動かしておきます。

Docker クライアントは Windows、 Mac にインストール。

仮想上の Linux で動く Docker と、 Windows や Mac で動く Docker クライアントが通信できるようにしてあげれば  
まるで Windows や Mac の上で Docker が動いているように見せかけられる。  
というカラクリでした。

****

6日目に出したこの図

![](images/docker-01.png)

これをより正確にすると

**Docker for Windows （Docker for Mac も同様）**

![](images/docker-02.png)

**Docker Toolbox**

![](images/docker-03.png)

このような構成を、それぞれのインストーラーで作成していたわけです。

****

なぜここまでして、Windows や Mac の上で Docker が動いているように見せかけるのかというと  
**開発環境と運用環境を全く同じ操作にする**ためです。

サーバーは Linux のシェアが圧倒的に高いのですが、クライアントは Windows や Mac というエンジニアが非常に多いです。  
そのようなエンジニアにとって、 OS による操作感の違いは非常にストレスになります。

エンジニアの支持を得るためにも、クロスプラットフォームで全く同じ操作感で使えるというのは非常に重要なわけです。

# まとめ
* Docker の機能
    - コンテナを簡単に作成できる
    - コンテナに展開するイメージを作成できる
    - イメージを保管するリポジトリがある
    - イメージを差分管理して軽量化する
    - クロスプラットフォームのクライアントツール
        - Docker と Docker クライアントは TCP で通信しているクラサバシステム
* Docker for Windows/Mac、Docker Toolbox 
    - Hyper-V や xhyve、 VirtualBox に Linux サーバーを立てる
    - Win/Mac に Docker クライアントをインストールする
    - Win/Mac の Docker クライアントから、仮想 Linux サーバーに接続する
    - Linux でしか動かない Docker を、まるで Windows や Mac で動いているように見せかける
        - 開発環境と運用環境で全く同じ操作になる

****

今日の記事でもちょいちょい布石を打ちましたが、次回からは「Docker は何を解決するか」という内容に入ります。  
前半戦のクライマックスです。