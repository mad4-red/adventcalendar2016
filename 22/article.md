OBIC Portal と Docker ～デプロイ戦略～
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の21日目の記事です。 

今日はデプロイの話。  
作ったアプリケーションをどうやってプロダクション環境に展開するかという点について書きます。

# 継続的デリバリー
OBIC Portal はその特性上、 OBIC の都合でサービスの仕様を変えやすいサービスです。  
「Twitter がまた仕様変えてきた」とか、よくありますよね。それをやれる側になるわけです。

もちろん好き勝手に変更を加えるのではなく、ユーザーからのフィードバックを受けて改善するわけですが  
そのスピードが早ければ早いほど、ユーザーの満足度につながりやすいです。  
「前のほうがよかった」なんてこともよくある話ではありますが…

アプリケーションに日々改善を加え、いち早く本番環境に適用（デプロイ）する。  
これを「継続的デリバリー（CD）」と呼びます。

自動ビルドや自動テストによって、アプリケーションに日々改善を加えていくのが「継続的インテグレーション（CI）」。  
そこに、「本番環境に適用する」というフローを追加したものです。

CI ならば「開発」だけの話ですが、デプロイを含むとなると「運用」の側面を含みます。  
いわゆる DevOps ですね。

CD を実現するためのインフラ、そこに Docker の仕組みを利用します。

## Jenkins
CD の前に CI を実現しなければなりません。  
自動テストや自動ビルド。

これらに関しては TFS でできます。  
Chatter に [TFSビルド定義製作委員会](https://ap.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F910000000Xp3w) なるグループがあったので、今後の活動に期待。

TFS でできるのですが、 OBIC Portal だと問題があります。  
[20日の記事](http://tkysva448/Hinata/item/03d181354beb450e80f92cab7c9b7c8b)に使ったイメージを再利用すると

![](images/dev-01.png)

* ASP.NET Core アプリのビルド
* Docker イメージのビルド

この二つが「ビルド」なのですが

![](images/dev-02.png)

TFS サーバー上でビルドすることになるので、 TFS サーバーに .NET Core や Docker クライアントがないといけません。  
でも、権限がないのでインストールできません。

TFS はビルドサーバーとしては使えないわけです。困った。  
というわけで、別のビルドサーバーを立てます。

[Jenkins](https://jenkins.io/) です。

![](images/jenkins-01.png)

Jenkins は OSS の CI ツールです。  
独自のプラグイン機構を持ち、有志の作成したプラグインを使うことで多彩に拡張できるツールです。  
TFS からソースを取得するというプラグインもあります。

というわけで、 Jenkins のサーバーを立てて、そこに .NET Core と Docker をインストールします。  
そして、 Jenkins から TFS においてあるソースを取得するように設定します。

![](images/jenkins-02.png)

実際の Jenkins の画面はこんな感じ

![](images/jenkins-03.png)

で、 Jenkins にビルドをさせることで

![](images/jenkins-05.png)

ビルドを Jenkins に任せる構成の完成です。  
あとは、 Jenkins で「TFS にソースがチェックインされたら最新ソースを取得してビルドする」というタスクを組んであげれば自動ビルドの完成。

# Docker Registry
Jenkins を使って Docker イメージを作るところまでできました。  
次はその Docker イメージを使って、プロダクション環境にコンテナを立ち上げる構成を作るわけですが…

![](images/registry-01.png)

Docker のイメージを持ってきて、コンテナを立ち上げる。  
コマンドとしては、 `docker pull` と `docker run` (または、 `docker service create` ) です。

`docker pull` は Docker Hub からイメージを持ってくるコマンドだと以前書きました。  
今回、使うイメージは自前のものです。  
ではどうするか。  
自前で Docker Hub のようなものを用意します。

それが、 [Docker Registry](https://docs.docker.com/registry/) です。

Docker Registry も Docker 社の OSS で、 [GitHub](https://github.com/docker/distribution) にソースが公開されています。  
Docker Hub と互換性のある API を提供していて、自前の Docker イメージを保管することができます。

Docker Registry サーバーを用意して、そこに向けてイメージを `push` することで、自前 Docker Hub にイメージをストックします。  
プロダクション環境にコンテナを展開するときは、自前 Docker Hub から `pull` します。

![](images/registry-02.png)

これで、 Docker イメージをプロダクション環境に展開する構成を組めました。

# 実は１台のサーバーでやってる
この構成が最初から組めればいいのですが、前にも言った通り「開発用にもらった Linux サーバーは１台」です。  
なので、この仕組み、全部コンテナで実現しています。

![](images/oneserver-01.png)

プロダクション環境でのデプロイは、開発環境の Linux サーバーが自作自演してる感じです。  
あたかも外からイメージを `pull` してきているようにしています。

コンテナを使えば、一台のサーバーでも色々なサーバーの機能を詰め込めます。  
逆算すると、別々のサーバーに分けることも容易なわけです。  

****

もっとも、開発環境のスペックが弱くて、自動ビルドとかかけてるとすぐに死んでしまうのですが…
（CPU 1コア、メモリ 4GB なのできついきつい）

プアな環境で開発することで、パフォーマンスなどの問題点にも気づけると前向きに捉えることにしています。

# バージョン管理
アプリケーションを動かすとき、そのアプリケーションがどのバージョンで動いてるかって大事ですよね。  
OBIC7 も、不具合起きた時にパッチがどこまで当たってるかが明確かどうかで対応のスピードが全く変わりますし。

アプリケーションのバージョンはどこに持たせておくか。  
もちろん TFS のラベルや、ビルドバージョンなんかを持たせておくのは当然として  
もうひとつ、Docker イメージ自体にバージョンを持たせます。

[4日目](http://tkysva448/Hinata/item/5b329a9bdecc429aaafbfba50050c9d8)の記事でちょっと触れた、イメージの「タグ」でバージョンを表現します。

メッセージアプリの Docker イメージ名を *portal/message* とすると `docker build` するときに

```
$ docker build -t portal/message:1.0 .
```

とすることで、タグ付きのイメージを作ることができます。

コンテナを立てるときは

```
$ docker pull portal/message:1.0
```

と、タグまで指定してイメージを取得します。
これで、「どのバージョンのイメージをもとにコンテナが作られたか」がわかるわけです。

****

OBIC Portal で実際に Docker Registry に保管されているイメージはこんな感じ

![](images/tag-01.png)

![](images/tag-02.png)

Nightly Build しているので、毎晩ビルドした時間のタグをつけてイメージを管理しています。

# Immutable Infrastructure and Disposable Components の実践
アプリケーションを改修して、新しいバージョンをデプロイするとき  
OBIC7 なら「パッチ適用」となりますが、 OBIC Portal では「コンテナを入れ替える」ことで実現します。

![](images/disposable-01.png)

バージョン 1.0 のイメージからコンテナを作っている環境があったとして、 
バージョン 2.0 が出た場合

![](images/disposable-02.png)

もともとあったバージョン 1.0 のコンテナはばっさり消して、 バージョン 2.0 のイメージを作り直します。

これがまさに、[12日目の記事](http://tkysva448/Hinata/item/d508080bf2a34f5caa4dec60fdad9a85)の内容、 Immutable Infrastructure and Disposable Components です。

アプリケーションのみならず、昨日の記事のようにネットワークというインフラも含めて  
すべて、ソースコードと運用環境が一致する状態です。

「誰かが運用の途中で設定を変えていた」という事が起こらない世界。

開発担当としても、運用担当としても、これ以上ない安心感のある構成です。  
Docker イメージとコンテナを用いたデプロイ構成を組むことで、 Immutable Infrastructure and Disposable Components というアーキテクチャを実装することができるのです。

# ここまでまとめ
* OBIC Portal では Jenkins を使って CI 環境を構成している
* Docker Registry を自前 Docker Hub を構築して、そこにイメージを保管している
    - 自前 Docker Hub と表現しましたが、実際は「プライベート Docker リポジトリ」と呼びます
* プライベート Docker リポジトリからプロダクション環境にイメージを `pull` することで CD を実現している
* 開発環境はサーバー１台なので、 Jenkins も Docker Registry もコンテナで作っている
    - コンテナで作っておけば、サーバーを分けることも容易
* バージョン管理は Docker イメージのタグで実現している
* コンテナごと入れ替える構成にすることで Immutable Infrastructure and Disposable Components を実践している

# これからやってみたいこと
Docker を使ったデプロイの構成をとったことで、今はまだ実装していないですが、こんなこともできるなぁと思っていることがあります。

## カナリアテスト
昨日のネットワークの構成もあわせて実現できそうなのが「カナリアテスト」です。

![](images/appendix-01.png)

こんな感じで、一部のユーザー・一部アプリのみ先行して新しいバージョンを適用して反応を見るテストです。  

炭鉱で毒ガスが発生していないか確認するときに鳥のカナリアを連れていくことから来たテストの手法。  
カナリアは毒ガスで人間より早く死んでしまうので、先行して危険がないか確認するという意のシステムテストですね。

## A/B テスト
似たような感じのテストで「A/Bテスト」があります。

![](images/appendix-02.png)

同じアプリケーションだけど、レイアウトや動作などが違うバージョンをふたつ（AバージョンとBバージョン）用意。  
ユーザーごとに A と B を振り分けて、それぞれの反応を見るテストです。

効果的な広告レイアウトなどを調査するときなどによくやるようです。  
「どっちのレイアウトのほうがオペミスが少ないか」みたいなテストをする時とかに使えそう。

## Blue/Green デプロイ
サービスを無停止でバージョンアップするデプロイ手法に Blue/Green デプロイというものがあります。

![](images/appendix-03.png)

こんな感じで、メッセージアプリの動くコンテナを動かしている環境で、新しいバージョンをデプロイしたいとき。

![](images/appendix-04.png)

もとのバージョンのコンテナはそのままで、新しいバージョンのコンテナを用意します。  
で、リバースプロキシの向け先を変えてやる。

![](images/appendix-05.png)

向け先を変えたら、元のコンテナを削除します。

![](images/appendix-06.png)

リバースプロキシの部分にひと工夫いりますが、これで「無停止」でサービスのバージョンアップができました。  
また新しいバージョンが出たら、これの繰り返し。

これが Blue/Green デプロイです。

24時間365日無停止が求められるようなサービスだと効果的なデプロイ手法です。  
OBIC の場合はなかなか無いっちゃないですが。

****

別に Docker を使った環境でなくても、この構成は作ろうと思えば作れるのですが  
コンテナならではの軽量さと、Disposable Components であるで、リソースを無駄にせず簡単に Blue/Green デプロイできるようになりました。

もっとも、**データベース**が絡むバージョンアップだと、 Blue/Green デプロイは途端に難しくなるので  
アプリケーションの作りにもよりますが。