コンテナの現在 ～パブリッククラウド～
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の17日目の記事です。 

[16日目の記事](http://tkysva448/Hinata/item/654aa0ec8209419d8cba2308bf63c523)では、 Docker 以外のツールについて紹介しました。

今日は、Docker およびコンテナの実行環境を提供するパブリッククラウドを紹介します。  
今回もかるーくです。

そもそも Docker の開発元である Docker 社、その前身である dotCloud 社は、アプリケーションの実行環境を提供する PaaS を事業としていました。  
dotCloud は PaaS 事業から Docker へとシフトしましたが、時代が Docker に追いついて、「コンテナ実行環境を提供するサービス」が登場してきました。

今回はその中でもメインどころ二つ、 Azure と AWS について説明します。


# Azure
我らが Microsoft の Azure では [Azure Container Service](https://azure.microsoft.com/ja-jp/services/container-service/) というコンテナホスティングサービスを提供しています。

実際に Azure Portal から見るとこんな感じ

![](images/azure-01.png)

Marketplace から選択して作成することができます。  
※個人アカウント。お金なくなっちゃうので実際作成して使ってみるところまでは試してません。

サービスの構成を決めていく中で面白いのが

![](images/azure-02.png)

Orchestrator Configuration というとこで選択できるのが

* DC/OS
* Kubernetes
* Swarm

となっているところです。  
※DC/OS は昨日の記事で出てきた Apache Mesos、Marathon をベースとしたエンタープライズ向けのコンテナ＆クラスター管理ツールです。

コンテナ自体は Docker を、クラスターの構成は Docker Swarm、 Kubernetes、 Apache Mesos など、使い慣れたものから選べるようになっています。

また、バックエンドで動いている OS は Windows も含まれているため、 Nano Server のような Windows Server 2016 でしか動かないコンテナも動かせるようになっています。

![](images/azure-03.png)

Azure のユーザーであり、Azure を使ってサービスを提供する人・企業にとっては、仮想マシンやクラスター環境の面倒は Azure が見てくれるので  
コンテナだけ気にしていればよくなります。

****

当然ですが、環境が雲の中にあったとしても、 Docker クライアントからコマンドラインで操作することができます。  
Azure SQL Database を SQL Server Management Studio から操作するのと同じ理屈です。

# AWS
もう一つ、パブリッククラウドの代表格、Amazon Web Service では [EC2 Container Service  (ECS)](https://aws.amazon.com/jp/ecs/) というコンテナホスティングサービスを提供しています。  
「EC2」は Elastic Compute Cloud という仮想マシンを提供するサービス。つまり、IaaS。  
それをベースとした「コンテナ実行環境だけを提供するサービス」が「ECS」です。

AWS の管理コンソールから操作できます。  
※ちなみに個人アカウント。「無料枠」の指定されているサービスなら12か月無料で試せます。

![](images/aws-01.png)

![](images/aws-02.png)

ひとつ、ないしは複数んコンテナのまとまりを「タスク」と呼んでいて、タスクを定義したあと、その定義から「サービス」を ECS のクラスター環境に展開します。  
タスクの定義はこんな感じ  

![](images/aws-03.png)

****

クラスター構成の管理機能は ECS が内包しています。

![](images/aws-04.png)

# IaaS +α
もちろんですが、 IaaS を使って仮想マシンを立てたうえに、コンテナのクラスタ環境を独自で構築することもできます。

Azure なら

![](images/plus-01.png)

どういう構成するかは

* サービスの性質
    - ベンダーがどこまで責任を持つか
* 価格
* エンジニアの技術力

などなど、いろいろな要素を考慮して決定すべきで、どちらが良いというわけでもありません。  
もちろん、自社のデータセンターで運用する選択もありです。

# おまけ：OpenStack と OpenShift
「自社のデータセンターで運用する」という点について、おまけ。  
自社のデータセンターにあるリソースを、クラウド上にあるように扱える技術があります。  

例えば、 Microsoft の提供する Azure Stack (https://www.microsoft.com/ja-jp/cloud-platform/products-Microsoft-Azure-Stack.aspx) 。  
オンプレミスのリソースを使って、 Azure と同じ体験を提供する製品です。

****

今回、自社クラウドを構築・運用する製品として今回紹介するのは、 OSS の OpenStack と OpenShift。

## OpenStack
[http://www.openstack.org/](http://www.openstack.org/)

自社のリソースをベースに、 IaaS 環境を構築運用する OSS です。

仮想マシンの作成、仮想マシン用のイメージの管理、ネットワーキング、オブジェクトストレージなどの機能を有しています。

## OpenShift
[https://www.openshift.com/](https://www.openshift.com/)

自社のリソースをベースに、 PaaS 環境を構築する OSS です。

アプリケーションの実行環境（Java、Ruby、PHP、Node.js、Python、Perlなど）や、データベース（MySQL、MongoDB、PostgreSQLなど）を提供します。  
また、コンテナの実行環境を提供する機能を有しています。

## OpenStack と OpenShift で自社内コンテナホスティングサービス

![](images/openstack-openshift-01.png)

こんな感じで、 OpenStack と OpenShift を組み合わせることで独自のコンテナホスティングサービスを構築することができます。

# まとめ
* AWS
    - EC2 Conteinare Service
* Azure
    - Azure Container Service
* IaaS + α
    - クラスター構成の構築運用は利用者の責任
    - IaaS を使えば実質どこでも可
* OpenStack + OpenShift
    - OpenStack
        - 自社のリソースをもとに IaaS 環境を構築・運用する
    - OpenShift
        - 自社のリソースをもとに PaaS 環境を構築・運用する
            - コンテナの実行環境も提供する
