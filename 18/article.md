私はなぜ Docker を採用したか
====

この記事は [Advent Calendar 2016 Extra Docker](http://192.168.3.175/Adventar/2016WinterExtra#/docker) の18日目の記事です。 

ここまでの記事で Docker 、コンテナについて一通りの内容に触れてきました。  
ここからは実際に Docker を使ったプロダクトの話をしていきます。

****

これまでは一般的な話をしてきましたが、いよいよここからは私の視点から記事を書いていきます。  
立場上色々ございますので、今日以降の記事はいつものお約束を唱えさせてください。

**この記事の内容は個人の見解であり、所属する組織・チームを代表するものではありません。**

# OBIC Portal というプロジェクト
まずもって、「OBIC Portal」って何よ。という話から。  

OBIC Portal は現在進行形で開発を進めているプロジェクトです。  
2015年11月に企画が持ち上がり、当時は「コミュニケーションポータル」という仮称でした。  
「1 : N のサービスをユーザーに提供する」ことを目的に、ユーザーと OBIC のコミュニケーションを行う場所を提供するサービスです。  
<small>この辺の情報は過去の製版研修会で発表されています。</small>

同年12月に私に「こういう仕組み、興味ない？」と声がかかり、「是非に」と承諾。  
<small>声がかかったのは、Yammer、 Hinata などの個人的な活動からなんとなくとがった仕組みに詳しそうなイメージと、現場 SE としての経験が買われたものと勝手に想像してます。</small>

****

現状、 SE とお客様とのやりとりは

* メール
* 電話
* Excel による資料

が主です。  
とくに、メールや電話によるやりとりは、担当者同士で完結しそこから広がることはありません。  

メールや電話でのやり取りを OBIC の提供するサービスに集約することで

* 属人化解消
* サービスレベルの向上・標準化
* 作業効率化
* トラブルの未然防止

などなどの効果を目指しています。

****

声がかかったのは昨年12月でしたが、私も物件の本稼働やら翌年に入ってからは6月売案件もあり、なかなか手を付けられず…  
ようやく取り掛かれたのが 2016年6月ぐらいから。  
皆様のもとにまだ届けられていないのが大変遺憾ではありますが、いち早くサービスインできるよう頑張ります。

私以外にも、羽鳥先生、山本啓介氏がプロジェクトに参画してくれています。  
その中で私が担当しているのが、「インフラ周り」。

そこで Docker との関わっていくことになりました。

## 現段階で有している機能

OpenID Connect を使用した ID 管理  
![](images/portal-01.png)

ポータル  
![](images/portal-02.png)

メッセージ  
![](images/portal-03.png)

ファイル共有  
![](images/portal-04.png)

タスク管理  
![](images/portal-07.png)

以降、アプリケーションとしてこれらを指すときは

* IdP
* Portal
* Message
* File
* Task

と表現します。

なお、これらアプリケーション部分に関しては深く触れません。  
私、インフラ周りの担当なので。

## OBIC Portal のインフラに求められる要件
OBIC Portal はサービスの性質から「お客様すべてに使われてなんぼ」の仕組みです。  
なので

* インフラは OBIC が保有する
    - オンプレは考慮しない
* お客様のプラットフォームを限定しない
    - 現時点では WEB サービスが最も適している

さらに

* 全ユーザーに展開することを見据え、スケールアウトする構成
* サービスの利用自体がユーザーの負担になってはいけない
    - 要は「お金を取りづらい」
    - もっと言うと「無料」で提供されるべきサービス
        - に見せる必要がある（無料とは言ってない）

という特徴があります。

****

なので、サービスの構築・運用においては、可能な限り「原価」をかけない構成が必要となります。  
もちろん、サービスを提供する上で必要な部分に関してはお金をかける判断は必要ですが。

# Linux を採用する理由
「原価をかけない」という点。  
この原価において大きな要素が「ライセンス料」です。

OBIC のサービスの主要プラットフォーム、 Windows を利用する場合、このライセンス料がかかってしまいます。  
原価を抑えるにはここからまずメスを入れる必要がありました。

****

Microsoft の詳しいライセンス体系については省略します。（というか、難しくていまだに頭が追いつかない）  
ただ、個人的な感覚は「オンプレは年々高くなっていく」です。

Microsoft のビジネスは完全に Azure へと軸足を移しています。  
SQL Server をはじめとして、「Azure で先行して作成した機能をオンプレに移植する」というフローができ上がっているぐらいです。

Microsoft はオンプレのユーザーを Azure に移行させたい。  
となれば、つぎに打つ戦略は決まってます。  
オンプレの価格を高くして、 Azure へと誘導する。  
です。

なので、これから新しくサービスを作る場合は

* 最初から Azure で構築する
* Microsoft 以外で行く
* オンプレでそれ以上のコストメリットを出す

という判断をする必要があると思います。

今段階で、 Azure に対する評価は出せていません。  
なので、「Microsoft 以外で行く」というのが妥当な線です。

****

Microsoft 以外で行く。  
すなわりサーバー OS に Windows 以外を選択する。  
そして、ライセンス料がかからない OS 。

世間的なシェアを鑑みても、 Linux 一択です。  

もちろん、 Linux を選択するということはそれだけ「自分たちで何とかしなければならない責任範囲が増える」ことでもあります。  
要するにそれだけ人件費という原価につながります。

この辺は、 RedHat のような優良サポートを選定することも視野に入れなければなりません。

そこは承知の上で、まずは「確実にかかる人件費以外のコストを Linux を採用することで下げる」という選択をしました。

## 謝辞
OBIC Portal では Linux を採用することにしました。

ただ、**「根本的に Linux サーバーの運用ノウハウが不足している」**というのが現状です。  
たとえば、セキュリティ対策はどうするとか、アカウント管理はどうするとか。  
ここについては、現時点で鋭意蓄積中。

そういうこともあって、社内の仮想環境に Linux を立てて貸し出すということが難しいのが現状です。  
OBIC Portal の開発環境も、特例的に構築してもらった 1 台の Linux サーバーでやりくりしています。  
その辺は、なにとぞご容赦ください。

## データベース
ちなみに、データベースは PostgreSQL を採用しています。  

ただ、「データベース」をはじめ「データの永続化」が関わる部分については、現段階でコンテナの責任範囲から除外する選択をしました。  
なので、今日以降の記事ではデータベース自体の内容についてはあまり触れません。あらかじめご了承ください。

Software Defined Storage（SDS）という分野が将来的にコンテナと密接に結びついてくると予想しています。  
この話はまたいずれ、機会があれば。

# コンテナを採用する理由
Linux をサーバー OS として採用することで、選択肢のひとつとして「コンテナ型仮想化」が増えることになります。  
次は、なぜコンテナを採用する必要があるかという話。

## ユーザーごとの環境をどう構築するか
たとえば、「サポートサイト」。  
FAQ やマニュアル、パッチなどを提供するこのサービスは、「ユーザーごと」という要素がないので「単一のサービス」として構築しても問題ありません。  
<small>詳しい内部構造は知らないのですが</small>  

「OBIC7 クラウド」はどうかというと、ユーザーごとに環境が分かれています。  
ユーザーごとにサーバーが分かれていて、データベースも分かれていて、ASecurityも別（グループ会社などは一緒だったりしますが）。

****

OBIC Portal は、「ユーザーごとにやり取りをする」サービスです。  
サービスとしては、個々のユーザーごとにサービスが区切られている必要があります。  
A社の人は、B社の情報を見られないようにする。といったセキュリティ上の担保が必要です。

それを実現する方法はふたつ。

* アプリケーションで実現する
* インフラで実現する

アプリケーションで実現する場合は、アクセスしたユーザーのプロファイルをもとにアクセスできる情報を制限するロジックを組み込むことになります。  
その分、アプリケーションが担う責任の範囲が広くなります。  

「アプリケーションの責任範囲を広げたくない」  
という思いがあり、まずは「アプリケーションで実現する」選択肢を除外。  
なぜアプリケーションに責任を負わせたくないかについては、明日の記事で。

で、「インフラで実現する」という選択になります。  
インフラで実現する一例は、先ほど挙げた「OBIC7 クラウド」のように、サーバーから分ける方法です。

## 開発時に少ないリソースを有効活用する
OBIC Portal のインフラ要件には「スケールアウトする構成」がありました。  
なので、「ユーザーごとにサーバーを分ける」という前提で「開発」をするとなると、最初から複数台の仮想サーバーを用意する必要があります。

そこで問題になるのが、「リソース」と「管理コスト」です。

物件用仮想サーバーと同様のリソースを使わせてもらうとなると、正味の話「物件用にとっておいてほしいので、あまり使いたくない」という気持ちがありました。  
なので、開発時に利用するリソースは最小限にしたかった。

では、小さいサイズの仮想サーバーを用意すればいいかというと、サーバーが増えれば増えるだけ管理コストがかかります。  
セキュリティ対策とかもろもろ。  

開発用にサーバーはいくつも欲しいけど、仮想サーバーはいくつも立てたくない。  

コンテナを使用することで、この問題を解決することができます。

## どこでも動かせる
OBIC Portal というサービスを作ったあと、「どこで動かすか」という問題が発生します。  

東京本社の 7F？  
データセンター？  
Azure？  
AWS？

外部とのやり取りが発生するので、どこで動かすかは重要かつ慎重に検討する必要があります。  

で、

* どこで動かすか決まらないと開発しづらい
* アプリケーションが実際にないとどこで動かすべきか試算しづらい

というデッドロック。

こういった問題もコンテナなら解消できました。  
コンテナで動く仕組みを作っておけば、コンテナのホストさえあればどこでも動かすことができます。  
なので、「最初は東京の7Fで社内向けに公開して、規模が拡大したらデータセンターに移して」といった柔軟な動きが取れます。

普通の仮想サーバーでも同じことはできそう、と思われるかもしれませんが
コンテナ ＋ クラスター化技術（Docker Swarm mode や Kubernetes）を用いることで、「ネットワークすらそのまま移行できる」環境を用意することができます。

# Docker を採用する理由
そんなこんなで OBIC Portal においてコンテナを採用するメリットはご理解いただけたかと。

では、コンテナを扱う上でなぜ Docker を採用したかという点ですが。

ぶっちゃけると  
「Docker しか知らなかった」  
だけです。

というか rkt とか知ってましたが、 Docker の情報量が突出していて、世の中 Docker 以外の選択肢がないように見える状態。

前章で「コンテナを採用する理由」とか書きましたけど、そんなのは後付けでまずは「Docker を使う」が先にあったというのが実情です。

****

という話で終わると元も子もないので、「Docker 以外のコンテナ関連技術を使うべきか悩んで、やっぱり Docker で行くことにした理由」について記しておきます。

Docker 以外のコンテナ関連技術を使うか悩んだ点は、クラスター構成をどうするかという点でした。  

Docker によって開発環境（サーバー一台）におけるコンテナの利用は楽に実現できました。  
それを、実際の運用を見越して複数台のサーバー構成にしたときにどうするか。

私がプロジェクトに取り掛かれたのが、実質今年の6月。  
その時点で候補に挙がるのが Kubernetes です。  
Google 謹製で、それなりに運用ノウハウの情報もネットから取得できました。  

その直後、 Docker 1.12 で Swarm mode が搭載されます。

****
Kubernetes か Swarm mode か。

この2択で、 Swarm mode を選択しました。  
理由は「学習コストの低減」です。

Kubernetes は実績もあり実運用という面では優位です。  
ただ、習得が難しい。

反面 Docker Swarm mode は、実績はないが今までの Docker の延長線上にあるため学習コストが低い。  
そもそも Docker は Docker 社曰く「難しいものを簡単にした仕組み」というのがウリです。  

学習コストの面から、 Docker 一本でいくことを決意しました。

****

学習コストの話は、私個人だけの話ではありません。  
私以外の人材にも広く知ってもらい、使ってもらう必要があります。  

「運用担当がひとりしかいない」サービスはそのうち確実に破たんしてしまうので。

Docker 一本で行くと決意してからも、まだまだ不安定なところもあって幾度となく「Kubernetes を使うべきか、Docker 一本でいくべきか」と吐きそうになるぐらい思い悩み今に至ります。  
GitHub で Issue を見ては、既知のバグかどうかを確認したり…

将来的に「やっぱり Kubernetes や DC/OS 使おう」なんてことになる可能性はありますが  
現段階では「学習コスト」の一点で、 Docker 一本で行く選択をしています。

# これからの記事
そういうわけで、 OBIC Portal で Docker を使っているという大枠について書かせていただきました。

明後日の記事からはより深く、「Docker を使ってどのように OBIC Portal というサービスを構築しているか」について書いていきます。  
<small>明日の記事はまたちょっと別の話です</small>

あわせて、その中で使用しているその他の OSS についても触れていきます。  
色々出てきますが、それらすべて「学習コスト」という観点から、「本当に使うべきか」を吐きそうになりながら熟慮して採用しているつもりです。  
常に「やりすぎないように」という事を念頭においてきたつもりなので、ぜひ引かないでいただければ幸いです。
