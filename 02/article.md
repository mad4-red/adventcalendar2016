Dockerfile で自分用のイメージを作る
====

この記事は Advent Calendar 2016 Extra Docker の２日目の記事です。  

1日目の記事では、 Docker のインストールと、基本的なコマンドについて説明しました。  
2日目は

自分で **イメージ** を作る

という内容で、 Docker について見ていきます。

# Dockerfile
1日目の記事で、 **Nginx** という Web サーバーのコンテナを立てました。

![](images/nginx-01.png)  

ブラウザに表示されたこの画面は、 Nginx のデフォルトのトップページです。

IIS を有効かしたときの Default Web Site にアクセスすると  
![](images/iis-01.png)  
こんなページが表示されますが、これと同じようなものです。

IIS に比べるとだいぶあっさりとしてますが。

****

Web サーバーを立てたところで、コンテンツを入れておかないと何も意味を成しません。  
というわけで、 Nginx のトップページを別のものに差し替えてみます。

そこで登場するのが Dockerfile です。

## とりあえずやってみる
詳しい話はあとにして、とりあえずやってみます。  

## Dockerfile を作る
まずは Dockerfile を作ります。  
Dockerfile を作るのに必要なのは、テキストエディターだけです。  
メモ帳でも十分です。

では実物を

```Dockerfile
FROM nginx

COPY index.html /wwwroot/
COPY nginx.conf /etc/nginx/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```

Dockerfile とは別に、差し替えるトップページの index.html を用意します。

```html
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<div class="container">
    <h1>OBIC Advent Calendar 2016</h1>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
```

<s>面倒くさいので</s>記事が長くなるので、大幅に省略。

最後に、 nginx.conf というファイルを加えます。

```nginx.conf
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    
    sendfile        on;    
    keepalive_timeout  65;

    server {
        listen 80;
        server_name localhost;

        location / {
            root /wwwroot;
            index index.html;
        }
    }
}
```

nginx.conf は、 IIS でいうところの machine.config や web.config にあたります。  
Web サーバーの構成を定義するファイルです。

これらのファイルを、こんな感じでフォルダにおいてあげます。

![](images/nginx-02.png)  
ここでは `C:\Sandbox\Nginx` というフォルダに置きましたが、もちろんどこでも構いません。

これで準備完了。

### Dockerfile からイメージをビルド
再びコマンドラインに戻ります。

コマンドプロンプトを立ち上げて、まずはファイルを配置したディレクトリに移動します。

```sh
$ cd C:\Sandbox\Nginx
```

そして

```sh
$ docker build -t my_nginx_image .
```

と打つと

```
Sending build context to Docker daemon 4.608 kB
Step 1 : FROM nginx
 ---> abf312888d13
Step 2 : COPY index.html /wwwroot/
 ---> ae05f8d78110
Removing intermediate container ab3c6cdceaee
Step 3 : COPY nginx.conf /etc/nginx/
 ---> 4a3ebe2600e2
Removing intermediate container 05d685bc1d19
Step 4 : EXPOSE 80
 ---> Running in bde8ba4cabd7
 ---> 9a0999e678c2
Removing intermediate container bde8ba4cabd7
Step 5 : CMD nginx -g daemon off;
 ---> Running in 2c90abc7a30c
 ---> 80777616566f
Removing intermediate container 2c90abc7a30c
Successfully built 80777616566f
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
```

こんな風に出力されます。

この状態で

```
$ docker images
```

と打つと

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my_nginx_image      latest              80777616566f        7 minutes ago       181.5 MB
nginx               latest              abf312888d13        2 days ago          181.5 MB
```

1日目の記事で出てきた nginx というイメージのほかに、もうひとつイメージが増えました。

### 作ったイメージからコンテナを作る
イメージができたので、それをもとにコンテナを作ります。  
これは昨日のおさらい

```sh
$ docker run -d -p 8080:80 my_nginx_image
```

で、 `http://localhost:8080` を見てみると

![](images/nginx-03.png)

トップページが変わりました。

## なにをやったか
1日目では、 [Docker Hub](https://hub.docker.com/) というサイトから、 Nginx の**イメージ**をダウンロードしてきました。  
この Docker Hub というサイトには、 Nginx のほかにもいろいろなイメージが置かれています。  

PE の方には伝わると思いますが、感覚としては [Nuget Gallery](https://www.nuget.org/) みたいなものです。  

今回は Docker Hub にあるようなオープンなイメージではなく、自分で作ったイメージからコンテナを作成したわけです。

イメージを自分で作る際に必要なのが、 Dockerfile。
Dockerfile には、そのイメージがどういう構成かを定義しています。  

### Dockerfile の中身
作った Dockerfile の中身を見ていきます。

```Dockerfile
FROM nginx

COPY index.html /wwwroot/
COPY nginx.conf /etc/nginx/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```

まず、 `FROM`、`COPY`、`EXPOSE`、`CMD` などの、行頭にある大文字の単語たちを「命令」と呼びます。  
これらはあとで説明する `docker build` や、昨日出てきた `docker run` のコマンドを実行する際に、 Docker にやってもらうことを定義しています。

#### FROM
`FROM` では、「元になるイメージ」を指定します。  
Docker Hub に置いてある Nginx のイメージをもとに、命令を追加していくわけです。

#### COPY
ローカルにあるファイルを、コンテナの中にコピーする命令です。  

```Dockerfile
COPY index.html /wwwroot/
```

`index.html` はローカルのファイル、 `/wwwroot/` はコンテナの中のパスです。

#### EXPOSE
`EXPOSE` は、コンテナから見て 「何番のポートで通信するか」を指定しています。  

****
nginx.conf の中で `listen 80;` と書いてるところがありますが、これは「80番ポートで HTTP の通信をするよ」という定義になっています。

IIS でいうと、「バインド」に当たると思ってください。  
![](images/iis-02.png)

****
すごーく大雑把な説明をすると、 `EXPOSE` は「ファイアウォールを開ける」イメージです。  
`EXPOSE 80` と命令することで、「コンテナの80番ポートを開けておく」という状態になります。

****
「80番をあけておいたのに、ブラウザでアクセスするときは `http://localhost:8080` だったじゃないか」  
という疑問が出てきますが、これは昨日説明を端折った `docker run` の `-p` オプションによるものです。

```
$ docker run -d -p 8080:80 my_nginx_image
```

この `-p 8080:80` は 「8080番ポートでの通信は、コンテナの80番ポートに送るね」という意味合いだったのです。

この機能は**ポートフォワーディング**と呼びますが、最初はたぶん混乱しがちなポイントだと思います。  
「へぇ～」と聞き流していただいてもかまいません。

#### CMD
ここまでにあげた命令は、 `docker build` する際に実行される命令でした。  
`CMD` は、 `docker run` するときに実行される命令です。

```Dockerfile
CMD ["nginx", "-g", "daemon off;"]
```

これは、 「Nginx を起動するコマンドを実行する」という命令を表します。

#### その他
今回説明しなかった命令のほかにも `RUN`、 `ENTRYPOINT`、 `ADD` などがあります。  
たまたま今回のケースに出てこなかっただけで、 使う頻度が少ないわけではありません。

詳しくは Qiita に記事を書いてくれている人がいっぱいいらっしゃるので、興味が出てきたら探してみてください。  
もちろん [公式ドキュメント（英語ですが）](https://docs.docker.com/engine/reference/builder/) にも詳しく書いてあります。

### docker build
Dockerfile にイメージの構成を定義したので、実際にイメージを作り上げます。  
そこで登場するのが `docker build` コマンドです。

C# のプログラミングで例えると、 Dockerfile が `.cs` ファイルで、 `docker build` コマンドがそのものずばりビルド。  
出来上がる `.dll` や `.exe` がイメージみたいな感じです。

```sh
$ docker build -t my_nginx_image .
```

`-t` オプションで出来上がるイメージの名前を指定しています。  
最後の `.` はディスプレイのほこりではなく、 Dockerfile のある場所を指定しています。  

今回は事前に `cd` で Dockerfile のあるところに移動して実行したので、 「現在の場所」を指す相対パス `.` を指定しています。

# イメージとコンテナ
1日目に 「イメージとコンテナというものがある」 という大雑把なまとめ方をしておきながら、だいぶ踏み込んだ形になりました。

ただ、プログラミング経験がある方はすでにピンときていると思いますが

* Dockerfile = クラス
* イメージ = dll、exe
* コンテナ = インスタンス

と置き換えられそうな気がしませんか？

「プログラミングと同じように 『Webサーバー』 を立てた」 というと、どうでしょう？  
ちょっと面白みが出てきませんか？

****

すでに何回か登場している `docker run` コマンドでは、イメージをもとにコンテナを作ります。  
コンテナはすなわち「実体」というわけです。

イメージを一度作っておけば、 何回 `docker run` しても、**同じ状態の**コンテナが立ち上がります。
ここに Docker の魅力の一つがあるわけです。

例えば、 OBIC7 の Web システムを設定するとき

* IIS を有効にする
* インストーラーを配置する
* 全モジュールダウンロードする
* アプリケーションプールを作成する
* アプリケーション化する

といった作業が必要になりますよね。  

いろいろなツールのおかげで、どんどん作業は楽になっていますが、これを何十台ものサーバーにやるとしたらどうでしょう。

これらの作業を Dockerfile に定義しておいて、 `docker run` できたら…

この辺の話は、カレンダー中盤により掘り下げて書くつもりなので、お楽しみに。

# まとめ
* Dockerfile を作って自分でイメージを作成することができる
* Dockerfile にはイメージの**定義**を書く
* `docker build` コマンドで Dockerfile からイメージを作成する
* イメージをもとにコンテナ（実体）を作成する
* コンテナは `docker run` するたびに、毎回同じ状態で作成される

## 蛇足
* 当初、 DockerCompose というものについても説明しようと予定していたのですが、 <s>時間が無くなった</s> 今回の記事の趣旨とちょっと合わなくなったので外しました
    - どこかで書く隙間あるかなぁ…